


-- Mission Basic setup 1v1 player

function Initialise()

-- Sets up mines and turn time
    SetData("Mine.DudProbability", 0.1)
    SetData("Mine.MinFuse", 1000)
    SetData("Mine.MaxFuse", 5000)

    SetupTeams()
    SetupInventories()
    --SpawnCrate()
   SetWormpotModes()

    SetData("Camera.StartOfTurnCamera", "Default")
    StartFirstTurn()
    -- test random crate code (to check when new weapons are added, that they're also added to the scheme container) - PNT
    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HealthChance = 1
    scheme.WeaponChance = 1
    scheme.UtilityChance = 1
    scheme.MysteryChance = 1
    CloseContainer(lock)

	CreateSentryGun()
	StartTimer("SentryGunLookAt1", 10000)
	StartTimer("SentryGunLookAt2", 20000)
	StartTimer("SentryGunLookAt3", 30000)
	StartTimer("SentryGunLookAt4", 40000)
	StartTimer("SentryGunFireAt", 50000)
end

function CreateSentryGun()
	SetData("SentryGun.PositionObject", "sentryposition")
	SetData("SentryGun.TeamIndex", 1)
	SendMessage("GameLogic.CreateSentryGun")
end

function SentryGunLookAt1()
	SetData("SentryGun.LookAtObject", "sentrylookat1")
	SendMessage("SentryGun.LookAt")
	lib_Comment("Looking at target 1")
end

function SentryGunLookAt2()
	SetData("SentryGun.LookAtObject", "sentrylookat2")
	SendMessage("SentryGun.LookAt")
	lib_Comment("Looking at target 2")
end

function SentryGunLookAt3()
	SetData("SentryGun.LookAtObject", "sentrylookat3")
	SendMessage("SentryGun.LookAt")
	lib_Comment("Looking at target 3")
end

function SentryGunLookAt4()
	SetData("SentryGun.LookAtObject", "sentrylookat4")
	SendMessage("SentryGun.LookAt")
	lib_Comment("Looking at target 4")
end

function SentryGunFireAt()
	SetData("SentryGun.FireAtObject", "sentryfireat")
	SendMessage("SentryGun.FireAt")
	lib_Comment("Firing at target")
end

function SetupTeams()
    lock, GM = EditContainer("GM.GameInitData")
	GM.NumberOfTeams = 2 
	
	-- Team 1
	GM.T1_Name = "Team 1"
	GM.T1_NumOfWorms = 4
	GM.T1_W1_Name = "T1,W1"
	GM.T1_W2_Name = "T1,W2"
	GM.T1_W3_Name = "T1,W3"
	GM.T1_W4_Name = "T1,W4"
	GM.T1_HatAttachment = "WXFE.A.Hat.Pirate"
	--GM.T1_Skill
	--GM.T1_Grave
	--GM.T1_Flag
	--GM.T1_Speech
	GM.T1_IsLocal = true
	GM.T1_AlliedGroup = 0
	--GM.T1_Handicap

	-- Team 2
	GM.T2_Name = "Team 2"
	GM.T2_NumOfWorms = 4
	GM.T2_HatAttachment = "WXFE.A.Hat.Cowboy"
	GM.T2_W2_Name = "T2,W1"
	GM.T2_W2_Name = "T2,W2"
	GM.T2_W3_Name = "T2,W3"
	GM.T2_W4_Name = "T2,W4"
	--GM.T2_Skill
	--GM.T2_Grave
	--GM.T2_Flag
	--GM.T2_Speech
	GM.T2_IsLocal = true
	GM.T2_AlliedGroup = 1
	--GM.T2_Handicap
	
	CloseContainer(lock)
	SetupWormsAndTeams()
 end

 function SetupWormsAndTeams()

   WormIndex = 0
   TeamIndex = 0

   GM = QueryContainer("GM.GameInitData")

   if GM.NumberOfTeams>=1 then 
      SetupTeam(GM.T1_Name, GM.T1_NumOfWorms, GM.T1_W1_Name, GM.T1_W2_Name, GM.T1_W3_Name, GM.T1_W4_Name, GM.T1_W5_Name, GM.T1_W6_Name,
               GM.T1_Skill, GM.T1_Grave, GM.T1_Flag, GM.T1_Speech, GM.T1_IsLocal, GM.T1_AlliedGroup, GM.T1_Handicap,GM.T1_HatAttachment, GM.T1_GlassesAttachment, GM.T1_TashAttachment, GM.T1_GlovesAttachment)
   end  
   if GM.NumberOfTeams>=2 then 
      SetupTeam(GM.T2_Name, GM.T2_NumOfWorms, GM.T2_W1_Name, GM.T2_W2_Name, GM.T2_W3_Name, GM.T2_W4_Name, GM.T2_W5_Name, GM.T2_W6_Name,
               GM.T2_Skill, GM.T2_Grave, GM.T2_Flag, GM.T2_Speech, GM.T2_IsLocal, GM.T2_AlliedGroup, GM.T2_Handicap,GM.T2_HatAttachment, GM.T2_GlassesAttachment, GM.T2_TashAttachment, GM.T2_GlovesAttachment)
   end
   if GM.NumberOfTeams>=3 then 
      SetupTeam(GM.T3_Name, GM.T3_NumOfWorms, GM.T3_W1_Name, GM.T3_W2_Name, GM.T3_W3_Name, GM.T3_W4_Name, GM.T3_W5_Name, GM.T3_W6_Name,
               GM.T3_Skill, GM.T3_Grave, GM.T3_Flag, GM.T3_Speech, GM.T3_IsLocal, GM.T3_AlliedGroup, GM.T3_Handicap,GM.T3_HatAttachment, GM.T3_GlassesAttachment, GM.T3_TashAttachment, GM.T3_GlovesAttachment)
   end  
   if GM.NumberOfTeams>=4 then 
      SetupTeam(GM.T4_Name, GM.T4_NumOfWorms, GM.T4_W1_Name, GM.T4_W2_Name, GM.T4_W3_Name, GM.T4_W4_Name, GM.T4_W5_Name, GM.T4_W6_Name,
               GM.T4_Skill, GM.T4_Grave, GM.T4_Flag, GM.T4_Speech, GM.T4_IsLocal, GM.T4_AlliedGroup, GM.T4_Handicap,GM.T4_HatAttachment, GM.T4_GlassesAttachment, GM.T4_TashAttachment, GM.T4_GlovesAttachment)
   end


    SendMessage("WormManager.Reinitialise")
end

function SetupTeam(TeamName, NumWorms, Worm1Name, Worm2Name, Worm3Name, Worm4Name, Worm5Name, Worm6Name,
                   Skill, Grave, Flag, Speech, IsLocal, AlliedGroup, Handicap,
                   HatAttachment, GlassesAttachment, TashAttachment, GlovesAttachment)



   local lock, team

   lock, team = EditContainer(lib_GetTeamContainerName(TeamIndex)) 
   team.Active = true
   team.Name = TeamName
   team.TeamColour = 0
   team.Skill = Skill
   team.GraveIndex = Grave
   team.FlagGfxName = Flag
   team.AlliedGroup = AlliedGroup
   team.ATT_Hat = HatAttachment
   team.ATT_Glasses = GlassesAttachment
   team.ATT_Tash = TashAttachment
   team.ATT_Gloves = GlovesAttachment
   team.IsLocal = IsLocal
   if Skill>0 then
      team.IsAIControlled = true
   else
      team.IsAIControlled = false
   end

   -- PC: added this to reset the win count for the teams if this is the first round played.
   local iRounds = GetData("GameOver.RoundNumber")
   if iRounds == 0 
   then 
	team.RoundsWon = 0
   end

   CloseContainer(lock) -- must close the container ASAP


   -- factor handicap into deciding worm health
   local scheme = QueryContainer("GM.SchemeData")
   local delta = 0
   if scheme.WormHealth == 100 then
      delta = 25
   elseif scheme.WormHealth == 150 then
      delta = 50
   elseif scheme.WormHealth == 200 then
      delta = 100
   end
   -- handicap will be set to either -1,0,1
   local Energy = scheme.WormHealth + (Handicap*delta)

   if NumWorms>=1 then 
      SetupWorm(Worm1Name, Speech, Energy) 
   end   
   if NumWorms>=2 then 
      SetupWorm(Worm2Name, Speech, Energy) 
   end
   if NumWorms>=3 then 
      SetupWorm(Worm3Name, Speech, Energy) 
   end
   if NumWorms>=4 then 
      SetupWorm(Worm4Name, Speech, Energy) 
   end
   if NumWorms>=5 then 
      SetupWorm(Worm5Name, Speech, Energy) 
   end
   if NumWorms>=6 then 
      SetupWorm(Worm6Name, Speech, Energy) 
   end


   -- set the inventory
   --CopyContainer("Inventory.Team.Default", GetTeamInventoryName(TeamIndex))

   --SetSpecialWeapon(TeamIndex, SWeapon)
   TeamIndex = TeamIndex+1

end


function SetupWorm(Name, Speech, Energy)
   
   local scheme = QueryContainer("GM.SchemeData")
   local lock, worm 

   lock, worm = EditContainer(lib_GetWormContainerName(WormIndex))
   worm.Active = true
   worm.PlayedInGame = true

   worm.Name = Name
   worm.TeamIndex = TeamIndex
   worm.SfxBankName = Speech    
   worm.WeaponFuse = 3
   worm.WeaponIsBounceMax = false
   worm.Spawn = "spawn"
   worm.ArtilleryMode = scheme.ArtileryMode
   worm.Energy = Energy

   CloseContainer(lock)

   WormIndex = WormIndex +1
   
end

function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
    inventory.Bazooka = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade =-1
    inventory.Airstrike = -1
    inventory.Dynamite = -1
    inventory.HolyHandGrenade = -1
    inventory.GasCanister = -1
    inventory.BananaBomb = -1
    inventory.Landmine = -1
    inventory.HomingMissile = -1
    inventory.Sheep = -1
    inventory.SuperSheep = -1
    inventory.Parachute = -1
    inventory.Jetpack = -1
    inventory.SkipGo = -1
    inventory.OldWoman = -1
    inventory.Girder = -1
    inventory.BridgeKit = -1
    inventory.Shotgun = -1
    inventory.GasCanister = -1
    inventory.NinjaRope = -1
    inventory.FirePunch = -1
    inventory.Prod = -1
    inventory.ConcreteDonkey = -1
    inventory.BaseballBat = -1
    inventory.Flood = -1
    inventory.Redbull = -1
    inventory.WeaponFactoryWeapon = -1
    inventory.Starburst = -1
    inventory.ChangeWorm = -1
    inventory.Surrender = -1
    inventory.SentryGun = -1
    inventory.Fatkins = -1
    inventory.PoisonArrow = -1
    inventory.Scouser = -1
    inventory.BubbleTrouble = -1
    inventory.SuperAirstrike = -1
    inventory.SniperRifle = -1
    inventory.AlienAbduction = -1
    

    CloseContainer(lock) -- must close the container ASAP

    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)

-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Team00")
   CopyContainer("Inventory.Team.Default", "Inventory.Team01")
   CopyContainer("Inventory.Team.Default", "Inventory.Team02")
end

function Crate_Collected()
end

function SpawnCrate()
    SetData("Crate.Spawn", "CrateSpawn1")
    SetData("Crate.Type", "Utility")
    SetData("Crate.Contents", "kUtilityCrateShower") 
    SetData("Crate.NumContents", 1)
    SetData("Crate.Index", 0)
    SetData("Crate.GroundSnap", 1)
    SendMessage("GameLogic.CreateCrate")
end

function DoOncePerTurnFunctions()
   SendMessage("GameLogic.StartMineFactory")
end

