function Initialise()


	SetData("TurnTime", 90000)
    SetData("RoundTime", 3000000)
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lock)    
    
	SendMessage("GameLogic.PlaceObjects")
	
	--SetData("Trigger.Visibility", 1)
	
	lib_SetupTeam(0, "Human_Team")
	lib_SetupTeam(1, "AI_Team")
	
	lib_SetupWorm(0, "Worm0")
	lib_SetupWorm(1, "Worm1")
	lib_SetupWorm(2, "Worm2")
	lib_SetupWorm(3, "Worm3")
	
	lib_SetupWorm(4, "Enemy0")
	lib_SetupWorm(5, "Enemy1")
	lib_SetupWorm(6, "Enemy2")
	lib_SetupWorm(7, "Enemy3")
        
        WormAILevel = "AIParams.CPU1"
        CopyContainer(WormAILevel, "AIParams.Worm04")
        CopyContainer(WormAILevel, "AIParams.Worm05")
        CopyContainer(WormAILevel, "AIParams.Worm06")
        CopyContainer(WormAILevel, "AIParams.Worm07")
        
	SendMessage("WormManager.Reinitialise")
    
	lib_SetupTeamInventory(0, "HumanWeapz")
	lib_SetupTeamInventory(1, "EnemyWeapz")
	
	BoomArray = { "Boom0", "Boom0b","Boom0c","Boom1", "Boom1b","Boom1c", 
                "Boom2", "Boom2b", "Boom2c", "Boom2d", "Boom3", "Boom3b",
                "Boom3c", "Boom3d", "JukeBoxTrig"}
	
	for i= 1, 15 do
		lib_SpawnTrigger(BoomArray[i])
	end
    
        lib_SpawnCrate("Target1")
        lib_SpawnCrate("Target2")
        lib_SpawnCrate("Target3")
        lib_SpawnCrate("Target4")
        
    Trig0Gone = false
    Trig1Gone = false
    Trig2Gone = false
    Trig3Gone = false
    
    TNT1Gone = false
    TNT2Gone = false
    TNT3Gone = false
    TNT4Gone = false
    
    AllPlayerWormsDead = false
    
    Turn = 0
    MidtrosPlayed = 0
    TrigCount = 0
    BaddieDead = 0
    HumanDead = 0
    EnemiesTakenDamage = 0
    ABaddieHasBeenHit = false
    PlayIntroMovie()
   
  
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
    
end

function EFMV_Terminated()
    
    
    -- find out which movie played last, check to see if
    -- its the last one if not add to the variable
    
    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        StartFirstTurn()

    elseif WhichMovie == "ExplosionScaffold" then
    
        MidtrosPlayed = MidtrosPlayed + 1
        FourMidtrosPlayed()
        
    elseif WhichMovie == "ExplosionShovel"  then
    
        MidtrosPlayed = MidtrosPlayed + 1
        FourMidtrosPlayed()
        
    elseif WhichMovie == "ExplosionChairs" then
   
        MidtrosPlayed = MidtrosPlayed + 1
        FourMidtrosPlayed()
      
    elseif WhichMovie == "ExplosionBar" then
    
        MidtrosPlayed = MidtrosPlayed + 1
        FourMidtrosPlayed()
        
    elseif WhichMovie == "Reminder" then
    
        SendMessage("EFMV.End")
    end
    
end


function FourMidtrosPlayed()
    
    --if all the midtros have been played then tag the Outro
    --onto the end of the last midtro
    if MidtrosPlayed == 4 then 
        
        SetData("EFMV.GameOverMovie", "Outro")
        --lib_DisplaySuccessComment()
        SendMessage("GameLogic.Mission.Success")
    
    else
    
        SendMessage("EFMV.End")
    end
end



function Trigger_Destroyed()
	
	TriggerIndex = GetData("Trigger.Index")
    TrigCount = TrigCount + 1     
    
    -- each time a trigger has been blown up, play the corresponding movie
    -- set the variable back to false so that the movies don't play every
    -- turn!!
    
    if TriggerIndex == 0 then
        
        SetData("EFMV.MovieName", "ExplosionScaffold")
        SendMessage("EFMV.Play")
        Trig0Gone = false

        
    elseif TriggerIndex == 3 then
        
        SetData("EFMV.MovieName", "ExplosionShovel")
        SendMessage("EFMV.Play")
        Trig1Gone = false
        
    elseif TriggerIndex == 6 then
        
        SetData("EFMV.MovieName", "ExplosionChairs")
        SendMessage("EFMV.Play")
        Trig2Gone = false
        
        
    elseif TriggerIndex == 10 then
        
        Trig3Gone = false
        SetData("EFMV.MovieName", "ExplosionBar")
        SendMessage("EFMV.Play")
        
    elseif TriggerIndex == 14 then
        
		lib_CreateExplosion("JukeBoxDead", 0, 1.35, 0, 40, 50)
	end
    
    -- if all TNT has gone but enemies are alive then give them
    --a hint message
--    if TrigCount == 5 and BaddieDead < 4 then
--        
--        lib_Comment("M.Con.Diner.COM.1")
--    
--    end
end

function Worm_Died()
    
    --if the index is higher than 3 then add to the counter
    deadWorm = GetData("DeadWorm.Id")
    
    if deadWorm < 4 then 
    
        HumanDead = HumanDead + 1
    
    elseif deadWorm > 3 then 
    
        BaddieDead = BaddieDead + 1
    end
    
    -- if all the baddies are dead but the TNT is still there
    -- give them a hint message
    if BaddieDead == 4 and TrigCount < 4 then
    
        SetData("EFMV.MovieName", "Reminder")
        SendMessage("EFMV.Play")
    
    elseif HumanDead == 4 then
        
        
        AllPlayerWormsDead = true
        
    end
end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        
        elseif AllPlayerWormsDead == true then
        
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        else
            
            CheckEnemyWormHealth()
            StartTurn()
        end
end
    

function CheckEnemyWormHealth()

    local Enemy0 = lib_QueryWormContainer(4)
    local Enemy1 = lib_QueryWormContainer(5)
    local Enemy2 = lib_QueryWormContainer(6)
    local Enemy3 = lib_QueryWormContainer(7)
    
    -- if any of the baddies take damage then add to variable
    
    
    if Enemy0.Energy < 70 then
    
        EnemiesTakenDamage = EnemiesTakenDamage + 1 
       
       
    elseif Enemy1.Energy < 70 then
    
        EnemiesTakenDamage = EnemiesTakenDamage + 1 
    
    
    elseif Enemy2.Energy < 70 then
    
        EnemiesTakenDamage = EnemiesTakenDamage + 1 
    
    
    elseif Enemy3.Energy < 70 then
   
        EnemiesTakenDamage = EnemiesTakenDamage + 1 
    end
end


function TurnStarted()
    
    Turn = Turn + 1
    
    
    if Turn > 1 and EnemiesTakenDamage == 0  then
    
        SetData("CommentaryPanel.Delay", 5000) -- 5 seconds
        SetData("CommentaryPanel.Comment", "M.Con.Diner.NEW.6")
        SendMessage("CommentaryPanel.TimedText")
    end
end
    
    
    
    
