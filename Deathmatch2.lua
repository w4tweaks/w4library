function Initialise()
    
    CopyContainer("DeathMatch02.SchemeData", "GM.SchemeData")
    
	SetData("TurnTime", 45000)
	SendMessage("GameLogic.PlaceObjects")
	
	--setup teams and worms from d/bank
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupTeam(1, "EnemyTeam")

    lib_SpawnCrate("Crate1") 
    lib_SpawnCrate("Crate2") 
    lib_SpawnCrate("Crate3") 	

	lib_SetupWorm(0, "Player2")
	lib_SetupWorm(1, "Player3")
	lib_SetupWorm(2, "Player4")
	
	lib_SetupWorm(3, "Enemy1")
	lib_SetupWorm(4, "Enemy2")
	lib_SetupWorm(5, "Enemy3")
	lib_SetupWorm(6, "Enemy4")

	SendMessage("WormManager.Reinitialise")

        WormAILevel = "AIParams.CPU1"
        CopyContainer(WormAILevel, "AIParams.Worm03")
        CopyContainer(WormAILevel, "AIParams.Worm04") 
        CopyContainer(WormAILevel, "AIParams.Worm05")
        CopyContainer(WormAILevel, "AIParams.Worm06")        
        
	lib_SetupTeamInventory(0, "Inv_Player")
	lib_SetupTeamInventory(1, "Inv_Enemy")
    
    lib_SetupTeamWeaponDelays(0, "PlayerDelays")
    lib_SetupTeamWeaponDelays(1, "EnemyDelays")

	PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
    StartFirstTurn()
end



function TurnEnded()
	lib_DeathmatchChallengeTurnEnded()
end

function DoOncePerTurnFunctions()

    SendMessage("GameLogic.DropRandomCrate")
end

