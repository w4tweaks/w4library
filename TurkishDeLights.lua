function Initialise()
	
	SetData("Land.Indestructable", 1)
    SetData("HotSeatTime", 0)
    
    --setup worms, teams and inv. from databank
    lib_SetupTeam(0, "TeamHuman")
    lib_SetupTeam(1, "TeamBaddie")
    lib_SetupTeam(2, "TeamPolice")
    lib_SetupTeam(3, "TeamChokey")

	lib_SetupWorm(0, "Player")
    lib_SetupWorm(1, "Player2") -- old index 12
    lib_SetupWorm(2, "Player3") -- old index 13
  
    lib_SetupWorm(3, "baddie1") -- old index 1
	lib_SetupWorm(4, "baddie2") -- old index 2
    lib_SetupWorm(6, "baddie3") -- old index 3

    lib_SetupWorm(5, "copper1")
    
    SendMessage("WormManager.Reinitialise")
    
    WormAILevel = "AIParams.CPU2"        
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    
    lib_SetupTeamInventory(0, "Inv_Human")  
    lib_SetupTeamInventory(1, "Inv_Baddie")  
    
    --spawn visible trigs
    --SetData("Trigger.Visibility", 1)
    
    --lib_SpawnTrigger("light1")
    lib_SpawnTrigger("light2")
    --lib_SpawnTrigger("light3")
    lib_SpawnTrigger("light4")
    
	lib_CreateEmitter("WXP_OilLamp", "Flame1")
	lib_CreateEmitter("WXP_OilLamp", "Flame2")
	lib_CreateEmitter("WXP_OilLamp", "Flame3")
	lib_CreateEmitter("WXP_OilLamp", "Flame4")
	
    lib_SpawnCrate("crate")
    
    --these will be set to true when the enemy 
    --worms are in the light areas of the map
    
    Worm1GotInLight = false
    Worm2GotInLight = false
    Worm3GotInLight = false
    
    Baddie1Dead = false
    Baddie2Dead = false
    Baddie3Dead = false
    
    Chokey1Dead = false
    Chokey2Dead = false
    Chokey3Dead = false
    
    BaddiesDead = 0
    CrateCollected = false
    --MidtrosPlayed = 0
    HumanDead = 0
    WormInLight = 0
    
    MovieMidtro1HasNotPlayed = false
    MovieMidtro2HasNotPlayed = false
    MovieMidtro3HasNotPlayed = false
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)    
    
    SendStringMessage( "Land.EnablePointLight", "PL01" )
    SendStringMessage( "Land.EnablePointLight", "PL04" )
    
    PlayIntroMovie()
    
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "TurkishDeLightsIntro")
    SendMessage("EFMV.Play")
end

--function PlayOutro()
--    SetData("EFMV.MovieName", "Outro")
--    SendMessage("EFMV.Play")
--end

function EFMV_Terminated()
    
    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "TurkishDeLightsIntro" then
    
        SendIntMessage("Worm.DieQuietly", 5)
        StartFirstTurn()
        
    end
    
    
    if WhichMovie == "MidtroCap1" then 
        
        SendIntMessage("Worm.DieQuietly", 5)
        --MidtrosPlayed = MidtrosPlayed + 1
        ThreeWormsGot()
        
    elseif WhichMovie == "MidtroCap2" then 
        
        SendIntMessage("Worm.DieQuietly", 5)
        --MidtrosPlayed = MidtrosPlayed + 1
        ThreeWormsGot()
        
    elseif WhichMovie == "MidtroCap3" then 
        
        SendIntMessage("Worm.DieQuietly", 5)
        --MidtrosPlayed = MidtrosPlayed + 1
        ThreeWormsGot()
    end
    
    
--    if WhichMovie == "Outro" then
--    
--        --lib_DisplaySuccessComment()
--        SendMessage("GameLogic.Mission.Success") 
--    end
    

   
end

function ThreeWormsGot()
    
    if BaddiesDead == 3 then 
        
        --EndTurn()
        SetData("EFMV.GameOverMovie", "Outro")
       
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    
    else
    
        SendMessage("EFMV.End")
    end
end



function Trigger_Collected()
    
    TriggerIndex = GetData("Trigger.Index")
    Worm = GetData("Trigger.Collector")
    
    -- if the worm is in the light set the variable to true 
    -- for turn ended function and add one to counter 
    
    if Worm == 3 then
        
        BaddiesDead = BaddiesDead + 1
        Worm1GotInLight = true
        WormInLight = WormInLight + 1
        lib_Comment("M.Arab.Turk.CON.1")
        SendIntMessage("Worm.DieQuietly", 3)
        lib_SetupWorm(8, "chokey1")
        SendIntMessage("Worm.Respawn", 8) 
        
        
    elseif Worm == 4 then 
        
        BaddiesDead = BaddiesDead + 1
        Worm2GotInLight = true
        WormInLight = WormInLight + 1
        lib_Comment("M.Arab.Turk.CON.2")
        SendIntMessage("Worm.DieQuietly", 4)
        lib_SetupWorm(9, "chokey2")
        SendIntMessage("Worm.Respawn", 9) 
       
    
    elseif Worm == 6 then 
        
        BaddiesDead = BaddiesDead + 1
        Worm3GotInLight = true
        WormInLight = WormInLight + 1
        lib_Comment("M.Arab.Turk.CON.3")
        SendIntMessage("Worm.DieQuietly", 6)
        lib_SetupWorm(10,"chokey3")
        SendIntMessage("Worm.Respawn", 10) 
        
    end
    
    if TriggerIndex == 2 then
        
        lib_SpawnTrigger("light2")
        lib_CreateExplosion("light2", 0, 1.35, 0, 20, 0)
    else
        
        lib_CreateExplosion("light4", 0, 1.35, 0, 20, 0)
        lib_SpawnTrigger("light4")
    end
        
end

function TurnEnded()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif HumanDead == 3 then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    end
    
    if Chokey1Dead == true then
        
        lib_SetupWorm(8, "chokey1")
        SendIntMessage("Worm.Respawn", 8)
        Chokey1Dead = false
    
    elseif Chokey2Dead == true then
    
        lib_SetupWorm(9, "chokey2")
        SendIntMessage("Worm.Respawn", 9)
        Chokey2Dead = false
        
    elseif Chokey3Dead == true then
        
        lib_SetupWorm(10, "chokey3")
        SendIntMessage("Worm.Respawn", 10) 
        Chokey3Dead = false
        
    elseif CrateCollected == true then
    
        lib_SpawnCrate("crate")
        CrateCollected = false
    
    end
    
        StartTurn()
    
end

function Worm_Died()
    
    --if any nme worms are killed and they're not in the light 
    --then respawn them in -- if police worm is killed then game over
    DeadWorm = GetData("DeadWorm.Id")
    
    if DeadWorm == 3 and Worm1GotInLight == false then
        
        lib_SetupWorm(3, "baddie1")
        SendIntMessage("Worm.Respawn", 3) 
        Baddie1Dead = true
        
        
    elseif DeadWorm == 4 and Worm2GotInLight ==  false then 
        
        lib_SetupWorm(4, "baddie2")
        SendIntMessage("Worm.Respawn", 4)
        Baddie2Dead = true
        
       
    elseif DeadWorm == 6 and Worm3GotInLight == false then
       
        lib_SetupWorm(6, "baddie3")
        SendIntMessage("Worm.Respawn", 6) 
        Baddie3Dead = true
        
    
    elseif DeadWorm == 0 or DeadWorm == 1 or DeadWorm == 2 then
    
        HumanDead = HumanDead + 1
        
        
    end
    
    if DeadWorm == 8 then
        
        Chokey1Dead = true
            
    elseif DeadWorm == 9 then
        
        Chokey2Dead = true
        
    elseif DeadWorm == 10 then
        
        Chokey3Dead = true
    end
end


function Crate_Collected()
    
    CrateCollected = true
    

end



function DoOncePerTurnFunctions()

    if Worm1GotInLight == true then
    
        if MovieMidtro1HasNotPlayed == false then
        
            lib_SetupWorm(5, "copper1")
            SendIntMessage("Worm.Respawn", 5)
            SetData("EFMV.MovieName", "MidtroCap1")
            SendMessage("EFMV.Play")
        end
        
    elseif Worm2GotInLight == true then
    
        if MovieMidtro2HasNotPlayed == false then
            
            lib_SetupWorm(5, "copper1")
            SendIntMessage("Worm.Respawn", 5)
            SetData("EFMV.MovieName", "MidtroCap2")
            SendMessage("EFMV.Play")
        end
        
    elseif Worm3GotInLight == true then
    
        if MovieMidtro3HasNotPlayed == false then
            
            
            lib_SetupWorm(5, "copper1")
            SendIntMessage("Worm.Respawn", 5)
            SetData("EFMV.MovieName", "MidtroCap3")
            SendMessage("EFMV.Play")
        end
    end
    
    if Baddie1Dead == true then
    
    
        lib_CreateExplosion("baddie1", 0, 1.35, 100, 20, 0)
        SetData("EFMV.MovieName", "BadGuyRespawn1")
        SendMessage("EFMV.Play")
         
    
    elseif Baddie2Dead == true then
    
       
        lib_CreateExplosion("baddie2", 0, 1.35, 100, 20, 0)
        SetData("EFMV.MovieName", "BadGuyRespawn2")
        SendMessage("EFMV.Play")
    
    elseif Baddie3Dead == true then
        
    
        lib_CreateExplosion("baddie3", 0, 1.35, 100, 20, 0)
        SetData("EFMV.MovieName", "BadGuyRespawn3")
        SendMessage("EFMV.Play")
    end    
    
    -- make sure if more than one worm hit a trigger that only one movie will be played
    Worm1GotInLight = false
    Worm2GotInLight = false
    Worm3GotInLight = false
    
    Baddie1Dead = false
    Baddie2Dead = false
    Baddie3Dead = false
end


    
    
        



