
-- Townhall

function Initialise()

    SendMessage("GameLogic.PlaceObjects")

    SendMessage("GameLogic.ResetTriggerParams")

    SetData("Camera.StartOfTurnCamera","Default")
    SetData("Mine.DudProbability", 0.1)
    SetData("Mine.MinFuse", 1000)
    SetData("Mine.MaxFuse", 5000)
    MaxWind = GetData("Wind.MaxSpeed")
    SetData("Wind.Speed", 1*MaxWind)
    SetData("Wind.Direction", 5.3)
   
--Triggers inside building used to check for amount of destruction done to building
        
    SetData("Trigger.Spawn", "Trig1")
    SetData("Trigger.Radius", 25)
    SetData("Trigger.Index", 1)
    SetData("Trigger.Visibility", 1)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig2")
    SetData("Trigger.Index", 2)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig3")
    SetData("Trigger.Index", 3)
    SendMessage("GameLogic.CreateTrigger")   

    SetData("Trigger.Spawn", "Trig4")
    SetData("Trigger.Index", 4)
    SendMessage("GameLogic.CreateTrigger") 

    SetData("Trigger.Spawn", "Trig5")
    SetData("Trigger.Index", 5)
    SendMessage("GameLogic.CreateTrigger")    
    
    SetData("Trigger.Spawn", "Trig6")
    SetData("Trigger.Index", 6)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig7")
    SetData("Trigger.Index", 7)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig8")
    SetData("Trigger.Index", 8)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig9")
    SetData("Trigger.Index", 9)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig10")
    SetData("Trigger.Index", 10)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig11")
    SetData("Trigger.Index", 11)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig12")
    SetData("Trigger.Index", 12)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig13")
    SetData("Trigger.Index", 13)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig14")
    SetData("Trigger.Index", 14)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig15")
    SetData("Trigger.Index", 15)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig16")
    SetData("Trigger.Index", 16)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig17")
    SetData("Trigger.Index", 17)
    SendMessage("GameLogic.CreateTrigger")
    
    SetData("Trigger.Spawn", "Trig18")
    SetData("Trigger.Index", 18)
    SendMessage("GameLogic.CreateTrigger")
    
    deadcount = 0
    trigcount = 0
    trigcollect = 0
           
    SetupWormsAndTeams()
    SetupInventories()
    StartFirstTurn()
end

--Checking for deadworms in order to spawn big weapon crates. Increases deadcount variable by one
--for each worm, this is used to decide which crate is spawned.
function Worm_Died()

    deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 4 or deadworm == 5 or deadworm == 6 or deadworm == 7 or deadworm == 8 or deadworm == 9 then
    
        deadcount = deadcount + 1
	CheckCrate()
    
    end

end


--Looks for destroyed triggers and adds 1 to trigcount variable for each trigger destroyed
--also chcks to see if all of townhall has been destroyed
--junk messages for debug purposes only
function Trigger_Destroyed()
	trigcount = trigcount + 1
	SetData("CommentaryPanel.Comment", "Trigger collected")
	SendMessage("CommentaryPanel.ScriptText")
    if trigcount == 18 then
	trigcollect = trigcollect +1
	SetData("CommentaryPanel.Comment", "Town Hall Destroyed")
	SendMessage("CommentaryPanel.ScriptText")
    end
      
end


function SetupWormsAndTeams()

    -- Teams --
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = GetData("User.HumanTeamName")
    team.TeamColour = 0
    CloseContainer(lock)

    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "The Darksiders"
    team.TeamColour = 1
    team.IsAIControlled = true
    CloseContainer(lock)
    
    -- Player Worms --
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = GetData("User.HumanWorm1")
    worm.Energy = 200
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    worm.Spawn = "HumanWorm1"
    CloseContainer(lock)
    
    CopyContainer("Worm.Data00", "Worm.Data01")
    lock, worm = EditContainer("Worm.Data01") 
    worm.Name = GetData ("User.HumanWorm2")
    worm.Energy = 100
    worm.Spawn = "HumanWorm2"
    CloseContainer(lock)

    CopyContainer("Worm.Data00", "Worm.Data02")
    lock, worm = EditContainer("Worm.Data02") 
    worm.Name = GetData ("User.HumanWorm3")
    worm.Energy = 100
    worm.Spawn = "HumanWorm3"
    CloseContainer(lock)

    CopyContainer("Worm.Data00", "Worm.Data03")
    lock, worm = EditContainer("Worm.Data03") 
    worm.Name = GetData ("User.HumanWorm4")
    worm.Energy = 100
    worm.Spawn = "HumanWorm4"
    CloseContainer(lock)

    --CPU Worms --
    CopyContainer("Worm.Data00", "Worm.Data04")
    lock, worm = EditContainer("Worm.Data04")
    worm.Energy = 75 
    worm.Name = "Agent Dennis"
    worm.TeamIndex = 1
    worm.SfxBankName = "Biggles"
    worm.Spawn = "CPUWorm1"
    CloseContainer(lock)

    CopyContainer("Worm.Data04", "Worm.Data05")
    lock, worm = EditContainer("Worm.Data05")
    worm.Name = "Chesh"
    worm.Spawn = "CPUWorm2"
    CloseContainer(lock)

    CopyContainer("Worm.Data04", "Worm.Data06")
    lock, worm = EditContainer("Worm.Data06")
    worm.Name = "Dunst"
    worm.Spawn = "CPUWorm3"
    CloseContainer(lock)

    CopyContainer("Worm.Data04", "Worm.Data07")
    lock, worm = EditContainer("Worm.Data07")
    worm.Name = "Das Bruce"
    worm.Spawn = "CPUWorm4"
    CloseContainer(lock)
    
    CopyContainer("Worm.Data04", "Worm.Data08")
    lock, worm = EditContainer("Worm.Data08")
    worm.Name = "Cyberpunk"
    worm.Spawn = "CPUWorm5"
    CloseContainer(lock)

    CopyContainer("Worm.Data04", "Worm.Data09")
    lock, worm = EditContainer("Worm.Data09")
    worm.Name = "Lil Burnt"
    worm.Spawn = "CPUWorm6"
    CloseContainer(lock)

    SendMessage("WormManager.Reinitialise")
end

function SetupInventories()
    lock, inventory = EditContainer("Inventory.Team00")
    inventory.Bazooka = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade = 5
    inventory.Dynamite = 2
    inventory.Landmine = 3
    inventory.Sheep = 1
    inventory.Jetpack = 2
    inventory.Parachute = 1
    inventory.SkipGo = -1
    inventory.Girder = -1
    inventory.Shotgun = 2
    inventory.Uzi = 2
    inventory.GasCanister = 1
    inventory.FirePunch = -1
    inventory.Freeze = 1
    inventory.Prod = -1
    inventory.PetrolBomb = -1
    CloseContainer(lock)

    CopyContainer("Inventory.Team00", "Inventory.Team01")
    
    lock, inventory = EditContainer("Inventory.Team01")
    inventory.Bazooka = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade = 5
    inventory.Dynamite = 1
    inventory.Landmine = 0
    inventory.Sheep = 0
    inventory.Jetpack = 0
    inventory.Parachute = 0
    inventory.SkipGo = -1
    inventory.Girder = 0
    inventory.Shotgun = 2
    inventory.Uzi = 1
    inventory.GasCanister = 0
    inventory.FirePunch = 0
    inventory.Freeze = 1
    inventory.Prod = -1
    inventory.PetrolBomb = -1
    CloseContainer(lock)

end

--Function used to spawn crates from deadcount variable
function CheckCrate()
	if deadcount == 1 then
		SpawnCrate1()
	end
	if deadcount == 2 then
		SpawnCrate2()
	end
	if deadcount == 3 then
		SpawnCrate3()
	end
	if deadcount == 4 then
		SpawnCrate4()
	end
	if deadcount == 5 then
		SpawnCrate5()
	end
	if deadcount == 6 then
		SpawnCrate6()
	end
end

--This is where we actually spawn the crates
function SpawnCrate1()
	if deadcount == 1 then
	        SetData("Crate.Spawn", "WCrate_SuperSheep")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 101)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponSuperSheep")
		SendMessage("GameLogic.CreateCrate")
	end
end


function SpawnCrate2()
	if deadcount == 2 then
	        SetData("Crate.Spawn", "WCrate_BananaBomb")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 102)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponBananaBomb")
		SendMessage("GameLogic.CreateCrate")
	end

end

function SpawnCrate3()
	if deadcount == 3 then
	        SetData("Crate.Spawn", "WCrate_HolyHandGrenade")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 103)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponHolyHandGrenade")
		SendMessage("GameLogic.CreateCrate")
	end

end

function SpawnCrate4()
	if deadcount == 4 then
	        SetData("Crate.Spawn", "WCrate_Airstrike1")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 104)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponAirstrike")
		SendMessage("GameLogic.CreateCrate")
	end

end

function SpawnCrate5()
	if deadcount == 5 then
	        SetData("Crate.Spawn", "WCrate_Airstrike2")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 105)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponAirstrike")
		SendMessage("GameLogic.CreateCrate")
	end

end

function SpawnCrate6()
	if deadcount == 6 then
	        SetData("Crate.Spawn", "WCrate_Airstrike3")
		SetData("Crate.Type", "Weapon")
		SetData("Crate.Index", 106)
		SetData("Crate.NumContents", 1)
		SetData("Crate.GroundSnap", 0)
		SetData("Crate.Contents", "kWeaponAirstrike")
		SendMessage("GameLogic.CreateCrate")
	end

end


--Success/Failure checks

function TurnEnded()
  
	TeamCount = lib_GetActiveAlliances()
	WhichTeam = lib_GetSurvivingTeamIndex()  

    if TeamCount == 0 then
            SendMessage("GameLogic.Mission.Failure")
    elseif TeamCount == 1 and WhichTeam == 1 then
            SendMessage("GameLogic.Mission.Failure")
    elseif TeamCount == 1 and WhichTeam == 0 and trigcollect == 1 then
            SendMessage("GameLogic.Mission.Success")
        
    elseif TeamCount == 1 and WhichTeam == 0 and trigcollect == 0 then
        StartTurn()
    else
        StartTurn()
    end
end

