function Initialise()
    
    CopyContainer("DeathMatch05.SchemeData", "GM.SchemeData")
    
	SetData("TurnTime", 40000)
	SendMessage("GameLogic.PlaceObjects")
	
	--setup teams and worms from d/bank
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupTeam(1, "EnemyTeam")

	
	lib_SetupWorm(0, "Player1")
	lib_SetupWorm(1, "Player3")
	lib_SetupWorm(2, "Player4")
	
	lib_SetupWorm(3, "Enemy1")
	lib_SetupWorm(4, "Enemy2")
	lib_SetupWorm(5, "Enemy3")
	lib_SetupWorm(6, "Enemy4")
	lib_SetupWorm(7, "Enemy5")
	lib_SetupWorm(8, "Enemy6")

	SendMessage("WormManager.Reinitialise")

        WormAILevel = "AIParams.CPU3"
        CopyContainer(WormAILevel, "AIParams.Worm03")
        CopyContainer(WormAILevel, "AIParams.Worm04")         
        CopyContainer(WormAILevel, "AIParams.Worm05") 
        CopyContainer(WormAILevel, "AIParams.Worm06")
        CopyContainer(WormAILevel, "AIParams.Worm07")
        CopyContainer(WormAILevel, "AIParams.Worm08")
        
        
	lib_SetupTeamInventory(0, "Inv_Player")
	lib_SetupTeamInventory(1, "Inv_Enemy")

    PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
    StartFirstTurn()
end


function TurnEnded()
	lib_DeathmatchChallengeTurnEnded()
end

function DoOncePerTurnFunctions()

    SendMessage("GameLogic.DropRandomCrate")
end

