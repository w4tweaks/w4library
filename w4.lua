local module_name = ...

--�������������� ������ ����������� �������
local old_Worm_Died            = Worm_Died
local old_Initialise           = Initialise
local old_EFMV_Terminated      = EFMV_Terminated
local old_Payload_Deleted      = Payload_Deleted
local old_TurnEnded            = TurnEnded
local old_Worm_Damaged         = Worm_Damaged
local old_Worm_Damaged_Current = Worm_Damaged_Current

function Worm_Died()
  if type(_G[module_name].onWormDied) == "function" then
    _G[module_name].onWormDied()
  else
    old_Worm_Died()
  end
end

function Initialise()
  if type(_G[module_name].onInitialise) == "function" then
    _G[module_name].onInitialise()
  else
    old_Initialise()
  end
end

function EFMV_Terminated()
  if type(_G[module_name].onEFMV_Terminated) == "function" then
    _G[module_name].onEFMV_Terminated()
  else
    old_EFMV_Terminated()
  end
end

function Payload_Deleted()
  if type(_G[module_name].onPayload_Deleted) == "function" then
    _G[module_name].onPayload_Deleted()
  else
    old_Payload_Deleted()
  end
end

function TurnEnded()
  if type(_G[module_name].onTurnEnded) == "function" then
    _G[module_name].onTurnEnded()
  else
    old_TurnEnded()
  end
end

function Worm_Damaged()
  if type(_G[module_name].onWorm_Damaged) == "function" then
    _G[module_name].onWorm_Damaged()
  else
    old_Worm_Damaged()
  end
end

module(module_name, package.seeall)