function Initialise()
	
	--SetData("Land.Indestructable", 1)
	SetData("TurnTime", 30000)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human Team")
	lib_SetupTeam(1, "AI Team")
	lib_SetupWorm(0, "Player")
	lib_SetupWorm(1, "CPU")
    lib_SetupWorm(2, "Dummy1")
	lib_SetupWorm(3, "Dummy2")
	
	lock, AIParams = EditContainer("AIParams.Worm01") 
	AIParams.TargetDetailObject = "Targ11"
	CloseContainer(lock)
        
	SendMessage("WormManager.Reinitialise")
	
	--SetData("Trigger.Visibility", 1)
	lib_SpawnTrigger("SafetyTrig")
    local EasterEgg= QueryContainer( "Lock.EasterEgg.2")
    if EasterEgg.State ~= "Unlocked"  then
       lib_SpawnTrigger("EasterEgg")
    end
	
	
	lib_SetupTeamInventory(0, "Inv Human")
	lib_SetupTeamInventory(1, "Inv CPU")
        
        WormAILevel = "AIParams.CPU2"
        CopyContainer(WormAILevel, "AIParams.Worm04")
        CopyContainer(WormAILevel, "AIParams.Worm05") 
        CopyContainer(WormAILevel, "AIParams.Worm06")
        CopyContainer(WormAILevel, "AIParams.Worm07")
	
    g_nAITargetId = 1
    g_nHumanTargetId = 1  
    g_bCrateDeletePhase = false
    
    BaddieDead = 0
    MidtroHasPlayed = false
    OutroIsPlaying = false
    GoldHasSpawned = false
    
    --lib_SpawnCrate("Gold")
    
    PlayIntroMovie()
    
    PlayerWormHasDiedAndTheOutroIsNotPlaying = false
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()

    local LastMoviePlayed = GetData("EFMV.MovieName")
    
    
    if LastMoviePlayed == "Intro" then
    
        SendIntMessage("Worm.DieQuietly", 2)
        SendIntMessage("Worm.DieQuietly", 3)
        StartFirstTurn()
    
    elseif LastMoviePlayed == "Midtro" then
    
        MidtroHasPlayed = true
        SendIntMessage("Worm.DieQuietly", 1)
        lib_CreateExplosion("BlockExplosion", 0, 1.35, 0, 70, 50)

    
    elseif LastMoviePlayed == "GoldCut" then 
        
        GoldHasSpawned = true
        
    end
end

function PlayMidtroMovie()

    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play")
    
end

function PlayOutroMovie()
    
    OutroIsPlaying = true
    
    lib_SetupWorm(8, "Player2")
    SendIntMessage("Worm.Respawn", 8) 
    SendIntMessage("Worm.DieQuietly", 0)
    SetData("EFMV.GameOverMovie", "Outro")
	
    SendMessage("GameLogic.Mission.Success")
    lib_DisplaySuccessComment()
end


function TurnStarted()
    
    if MidtroHasPlayed == false then
    
        -- At the start of the turn delete any remaining targets
        
        -- This variable stops us counting the targets destruction as a hit by player/cpu
        g_bCrateDeletePhase = true
        -- For this to work, all the targets need the same Id (0)
        SendIntMessage("Crate.Delete", 0)
        g_bCrateDeletePhase = false
        
        -- Create the target we want for this turn
        CreateTarget()
    end
end

function CreateTarget()
   -- At the start of each turn create the correct target

   local CurrentTeam = GetData("CurrentTeamIndex")
   if CurrentTeam == 0 then   
        local HumanCrateList = {"HTarg1", "HTarg2", "HTarg3", "HTarg4", "HTarg5",
                                "HTarg6", "HTarg7", "HTarg8", "HTarg9", "HTarg10"}
        
        lib_SpawnCrate(HumanCrateList[g_nHumanTargetId])
   else   
        local AICrateList = {"AITarg1", "AITarg2", "AITarg3", "AITarg4", "AITarg5",
                                "AITarg6", "AITarg7", "AITarg8", "AITarg9", "AITarg10"}
        
        lib_SpawnCrate(AICrateList[g_nAITargetId])
        
        lock, AIParams = EditContainer("AIParams.Worm01") 
		AIParams.TargetDetailObject = AICrateList[g_nAITargetId]
		CloseContainer(lock)
   end

end

function Crate_Destroyed()  

   -- The player did not hit the crate we are deleting it so ignore
   if g_bCrateDeletePhase then return end

   SetData( "Turn.Boring", 0 )
   SetData( "Turn.MaxDamage", 1 )

   local CurrentTeam = GetData("CurrentTeamIndex")
   if CurrentTeam == 0 then
        g_nHumanTargetId = g_nHumanTargetId + 1
        
        if g_nHumanTargetId > 10 then
        
        lock, worm = EditContainer("Worm.Data00") 
        worm.ArtilleryMode = false
        CloseContainer(lock)
        
        PlayMidtroMovie()
        
        
        lib_SetupWorm(4, "Bad1")
        SendIntMessage("Worm.Respawn", 4)
        lib_SetupWorm(5, "Bad2")
        SendIntMessage("Worm.Respawn", 5)    
        lib_SetupWorm(6, "Bad3")
        SendIntMessage("Worm.Respawn", 6)    
        lib_SetupWorm(7, "Bad4")
        SendIntMessage("Worm.Respawn", 7)    

    else
        CreateTarget()  -- create a new one now in case it was destroyed by the first shot of the shotgun
    end
   
   else
      g_nAITargetId = g_nAITargetId + 1
      if g_nAITargetId > 10 then
         
         
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
      else
        CreateTarget() -- create a new one now in case it was destroyed by the first shot of the shotgun
        
      end
   end 
end

function Worm_Died()
    
    --if TCW dies before the midtro is played then game over
    -- killin a "bad" worm adds to counter and once 4 have
    --been killed then player can move to collect gold
    
    DeadWorm = GetData("DeadWorm.Id")
    
    if DeadWorm == 1 and MidtroHasPlayed == false then
        
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    elseif DeadWorm > 3 then
    
        BaddieDead = BaddieDead + 1
    
    elseif DeadWorm == 0 and OutroIsPlaying == false then
        
        
        PlayerWormHasDiedAndTheOutroIsNotPlaying = true
        

    end
end


function Worm_Damaged()
    
    -- warns the player if they shoot TCW
    DamagedWorm = GetData("DamagedWorm.Id")
    
    if DamagedWorm == 1 then
    
        local worm = lib_QueryWormContainer(1)

    
        if worm.Energy < 90 then 
            
            SetData("CommentaryPanel.Delay", 3000)
            lib_Comment("M.Wild.Tin.CON.1")
            SendMessage("CommentaryPanel.TimedText")
        end
    end
end



function Crate_Collected()
    
    lib_DeleteEmitterImmediate(Emitter)
    PlayOutroMovie()
end


function TurnEnded()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    
    elseif PlayerWormHasDiedAndTheOutroIsNotPlaying == true then
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    else
        StartTurn()
    end
end


function Trigger_Destroyed()

    TrigIndex = GetData("Trigger.Index")
    
    if TrigIndex == 1 then
        
        SendStringMessage("WXMsg.EasterEggFound", "Lock.EasterEgg.2") -- unlock easter egg
        SetData("CommentaryPanel.Delay", 3000) -- 5 seconds
        SetData("CommentaryPanel.Comment", "M.Easter.1")-- You have found a easter egg
        SendMessage("CommentaryPanel.TimedText")
        
        lib_CreateExplosion("Boom1", 0, 1.35, 0, 20, 50)
        StartTimer("ToiletBang", 1000)
        
        Emitter = lib_CreateEmitter("WXP_Explosion_OilDrum", "Boom1") 
    end
end
        


function ToiletBang()
    lib_CreateExplosion("Boom2", 0, 1.35, 0, 20, 50)
    StartTimer("ToiletBang2", 1000)
    
    Emitter2 = lib_CreateEmitter("WXP_Explosion_OilDrum", "Boom2") 
end

function ToiletBang2()

    lib_CreateExplosion("Boom3", 0, 1.35, 0, 20, 50)
    Emitter3 = lib_CreateEmitter("WXP_Explosion_OilDrum", "Boom3") 
    
    StartTimer("DeleteAllEmitters", 6000)
end

function DeleteAllEmitters()
    lib_DeleteEmitter(Emitter)
    lib_DeleteEmitter(Emitter2)
    lib_DeleteEmitter(Emitter3)
end    

function DoOncePerTurnFunctions()
    
   -- make sure the gold only spawns once
    if BaddieDead == 4 and GoldHasSpawned == false then 
        
        
        lib_SpawnCrate("Gold")
        Emitter = lib_CreateEmitter("WXP_CollectableItem", "Party1") 
       
        SetData("EFMV.MovieName", "GoldCut")
        SendMessage("EFMV.Play")
    end
end
    
    