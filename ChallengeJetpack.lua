function Initialise()
	
    SetData("HUD.Counter.Active", 1) 
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
	SetData("HUD.Clock.DisplayTenths", 1)
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupWorm(0, "Player")
	lib_SetupTeamInventory(0, "Inv_Human")
	
	
	SendMessage("WormManager.Reinitialise")
	
	SetData("Trigger.Visibility", 1)

    TargetsLeft = 15
    g_nNextTarget = 1
	 ExtraFuel = 10000
     
    SetHudCounter()
    SpawnNextTarget()
   -- StartFirstTurn()
    SendMessage("Commentary.NoDefault")
   PlayIntroMovie()
   
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function Crate_Collected()

    SendMessage("Commentary.Clear")   

    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
    local myRandomInteger = lib_GetRandom(1, 4)
        
    lib_Comment(TextToDisplay[myRandomInteger])
    
    TargetsLeft = TargetsLeft - 1
    SetData("HUD.Counter.Value", TargetsLeft) 
    SpawnNextTarget()

end

function SetHudCounter()
    
    SetData("HUD.Counter.Value", TargetsLeft) 
end
    

function SpawnNextTarget()

    SendMessage("Jetpack.UpdateFuel")
    Fuel = GetData("Jetpack.Fuel")
    SetData("Jetpack.Fuel", Fuel + ExtraFuel)

    if g_nNextTarget == 16 then
    
        lib_DisplaySuccessComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
        
    else
    
    -- target cycle 
    local Crate = {"Targ1", "Targ2", "Targ3", "Targ4", "Targ5", "Targ6", 
    				"Targ7", "Targ8", "Targ9", "Targ10","Targ11", "Targ12", "Targ13", "Targ14", "Targ15"}
        lib_SpawnCrate(Crate[g_nNextTarget])
        g_nNextTarget = g_nNextTarget + 1
        
    log("g_nNextTarget = ", g_nNextTarget)
    
    end

end


function TurnEnded()
        StartTurn()
    
end


function Worm_Died()
    
    deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Failure")
    end
end
