function Initialise()
	
	SetData("Land.Indestructable", 0)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human_Team")
	lib_SetupWorm(0, "Director")
        lib_SetupWorm(1, "Assistant")
        lib_SetupWorm(2, "Stuntman")

        StartTimer("Drop", 51500)



	SendMessage("WormManager.Reinitialise")
    
        PlayIntroMovie()

end

function Drop()
        lib_SetupWorm(3, "Stuntman2")
        SendIntMessage("Worm.Respawn", 3)  
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
