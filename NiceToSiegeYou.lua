function Initialise()

	SetData("TurnTime", 30000)
	SetData("RoundTime", 900000)

    
	--triggers set up from databank
	SetData("Trigger.Visibility", 0)
	
    local EasterEgg= QueryContainer( "Lock.EasterEgg.1")
    if EasterEgg.State ~= "Unlocked"  then
       lib_SpawnTrigger("EasterEgg")
    end
	
--    lib_SpawnTrigger("Trigger2")
    
	--setup teams and worms from d/bank
	lib_SetupTeam(0, "Human Team")
	lib_SetupTeam(1, "CPU Team")
    lib_SetupTeam(2, "CPU_Archers")
    lib_SetupTeam(3, "ProfsTeam")
	
	lib_SetupWorm(0, "Worm1")
	lib_SetupWorm(1, "Worm2")
	lib_SetupWorm(2, "Worm3")
	lib_SetupWorm(3, "Prof") --prof worm
    
	lib_SetupWorm(4, "Enemy1")	
	lib_SetupWorm(5, "Enemy2")
	lib_SetupWorm(6, "Enemy3")
	lib_SetupWorm(7, "Enemy4")
    
    lib_SetupWorm(8, "Enemy5") -- catapult worm
	lib_SetupWorm(9, "Enemy6") -- catapult worm
	
    WormAILevel = "AIParams.CPU2"
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
    CopyContainer(WormAILevel, "AIParams.Worm08")
    CopyContainer(WormAILevel, "AIParams.Worm09")
    CopyContainer(WormAILevel, "AIParams.Worm10")
    CopyContainer(WormAILevel, "AIParams.Worm11")
    CopyContainer(WormAILevel, "AIParams.Worm12")
    CopyContainer(WormAILevel, "AIParams.Worm13")
        
	SendMessage("WormManager.Reinitialise")
    
    lib_SpawnCrate("Crate1")
    lib_SpawnCrate("Crate2")
    lib_SpawnCrate("Crate3")
    
	--inventories called from databank
	lib_SetupTeamInventory(0, "Human Inventory")
	lib_SetupTeamInventory(1, "CPU Inventory")
    lib_SetupTeamInventory(2, "Inv_Archer")
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 2000         
    CloseContainer(lock)
    
    Turn = 0
    CataWorm1DiedSoDoNotFire = false
    CataWorm2DiedSoDoNotFire = false
    PlayersWorms = 3 
    g_nNextCrate = 1
    
    
    Enemy5HasSpawned = false
    Enemy6HasSpawned = false
    
    ProfHasDiedOrHumanTeamAllDead = false
    IntroHasFinished = false
    PlayIntro()
  
end


function PlayIntro()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    
    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        
        IntroHasFinished = true
        
        SendIntMessage("Worm.DieQuietly", 3) -- kill of prof
        SendIntMessage("Worm.DieQuietly", 8)
        SendIntMessage("Worm.DieQuietly", 9)
        
        lib_SetupWorm(3, "ProfInGame")
        SendIntMessage("Worm.Respawn", 3) -- respawn him
       
        StartFirstTurn()
        
        
    elseif WhichMovie == "CatapultStrike" or WhichMovie == "CatapultStrike2" 
        or WhichMovie == "Easter Egg" or WhichMovie == "WormCut" or WhichMovie == "WormCut2" then
    
        SendMessage("EFMV.End")
        
    end
    
end


function Worm_Died()

    -- ignore this function in the intro (worms are killed off quietly)
    if Turn==0 then 
       return
    end

	deadWorm = GetData("DeadWorm.Id")
 	
    -- if Prof dies then its instant game over
    -- if enemy dies then respawn him
    -- if one of the cata. worms dies then set var. to true
    
    if deadWorm < 3 then
    
        PlayersWorms = PlayersWorms - 1
    end
    
    if deadWorm == 3 or PlayersWorms == 0 then 
        
        ProfHasDiedOrHumanTeamAllDead = true


--    elseif deadWorm > 3 then
--    
--        SpawnNextCrate()
    end
    
    if deadWorm == 4 then 
    
        lib_SetupWorm(10, "Enemy7")
        SendIntMessage("Worm.Respawn", 10) -- if worm 4 dies spawn worm 10
    
    
    elseif deadWorm == 5 then
    
        lib_SetupWorm(11, "Enemy8")
        SendIntMessage("Worm.Respawn", 11) -- if worm 5 dies spawn worm 11
    
    elseif deadWorm == 6 then 
    
        lib_SetupWorm(12, "Enemy9")
        SendIntMessage("Worm.Respawn", 12) -- if worm 6 dies spawn worm 12
    
    
    elseif deadWorm == 7 then
    
        lib_SetupWorm(13, "Enemy10")
        SendIntMessage("Worm.Respawn", 13) -- if worm 7 dies spawn worm 13
        
    
    elseif deadWorm == 10 then
    
        lib_SetupWorm(4, "Enemy1")
        SendIntMessage("Worm.Respawn", 4) -- if worm 10 dies spawn worm 4
    
    elseif deadWorm == 11 then
    
        lib_SetupWorm(5, "Enemy2")
        SendIntMessage("Worm.Respawn", 5) -- if worm 11 dies spawn worm 5
        
        
    elseif deadWorm == 12 then
   
       lib_SetupWorm(6, "Enemy3")
        SendIntMessage("Worm.Respawn", 6) -- if worm 12 dies spawn worm 6
        
    elseif deadWorm == 13 then
    
        lib_SetupWorm(7, "Enemy4")
        SendIntMessage("Worm.Respawn", 7) -- if worm 13 dies spawn worm 7
        
    
    
    
    elseif deadWorm == 8 then
    
        CataWorm1DiedSoDoNotFire = true
    
    elseif deadWorm == 9 then
    
        CataWorm2DiedSoDoNotFire = true
    end
end


function TurnStarted()

    Turn = Turn + 1
    
--    if Turn == 9 then 
--    
--        lib_Comment("M.Med.Nice.COM.1")
--    
--    elseif Turn == 19 then 
--    
--        lib_Comment("M.Med.Nice.COM.2")
--    end
    
        
end


function DoOncePerTurnFunctions()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
    
    
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
        
        SetData("EFMV.GameOverMovie", "Outro")
        
        SetData("WXD.StoryMovie", "WildWest")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    end
    
--    if Enemy5HasSpawned == true then
--    

--    
--    elseif Enemy6HasSpawned == true then
--    

--    end
    
    if Turn == 9  then
        
        
        lib_SetupWorm(8, "Enemy5")
        SendIntMessage("Worm.Respawn", 8)
        Enemy5HasSpawned = true
        SetData("EFMV.MovieName", "WormCut")
        SendMessage("EFMV.Play")
        Enemy5HasSpawned = false
    
    
    elseif Turn == 19 then
        
        
        lib_SetupWorm(9, "Enemy6")
        SendIntMessage("Worm.Respawn", 9)
        Enemy6HasSpawned = true
        SetData("EFMV.MovieName", "WormCut2")
        SendMessage("EFMV.Play")
        Enemy6HasSpawned = false
        
    end
    
    if Turn == 11 and CataWorm1DiedSoDoNotFire == false and ProfHasDiedOrHumanTeamAllDead == false then 
    
        SetData("EFMV.MovieName", "CatapultStrike")
        SendMessage("EFMV.Play")

    elseif Turn == 21 and CataWorm2DiedSoDoNotFire == false and ProfHasDiedOrHumanTeamAllDead == false then
    
        SetData("EFMV.MovieName", "CatapultStrike2")
        SendMessage("EFMV.Play")
    end
end

function TurnEnded()
    
    
    if ProfHasDiedOrHumanTeamAllDead == true then    
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    else
    
        StartTurn()
    end
end


--function SpawnNextCrate()
--    
--    -- crate weapon cycle 
--    local Crate = {"Crate1", "Crate2", "Crate3", "Crate4"}
--        lib_SpawnCrate(Crate[g_nNextCrate])
--        g_nNextCrate = g_nNextCrate + 1
--        
--    if g_nNextCrate > 4 then 
--        g_nNextCrate = 1 
--    end
--    
--end

function Trigger_Collected()
--    
--    -- if the player enters the secret entrance then trigger easter
--    -- egg efmv
 
    TrigIndex = GetData("Trigger.Index")
   
        if TrigIndex == 1 then
        
        lib_SetupWorm(14, "Cuckoo")
        SendIntMessage("Worm.Respawn", 14)
        
        SendStringMessage("WXMsg.EasterEggFound", "Lock.EasterEgg.1") -- unlock easter egg
      
        SetData("EFMV.MovieName", "Easter Egg")
        SendMessage("EFMV.Play")
    end
end
    
        
    

