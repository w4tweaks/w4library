function Initialise()


    CopyContainer("DeathMatch09.SchemeData", "GM.SchemeData")

	SetData("TurnTime", 30000)
	SendMessage("GameLogic.PlaceObjects")
	
	--setup teams and worms from d/bank
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupTeam(1, "EnemyTeam")

	
	lib_SetupWorm(0, "Player1")
	lib_SetupWorm(1, "Player2")
	lib_SetupWorm(2, "Player3")
	lib_SetupWorm(3, "Player4")
	
	lib_SetupWorm(4, "Enemy1")
	lib_SetupWorm(5, "Enemy2")
	lib_SetupWorm(6, "Enemy3")
	lib_SetupWorm(7, "Enemy4")
	lib_SetupWorm(8, "Enemy5")
	lib_SetupWorm(9, "Enemy6")
	lib_SetupWorm(10, "Enemy7")
	lib_SetupWorm(11, "Enemy8")
    
    
    
	SendMessage("WormManager.Reinitialise")

        WormAILevel = "AIParams.CPU5"
        CopyContainer(WormAILevel, "AIParams.Worm04")
        CopyContainer(WormAILevel, "AIParams.Worm05") 
        CopyContainer(WormAILevel, "AIParams.Worm06")
        CopyContainer(WormAILevel, "AIParams.Worm07")
        CopyContainer(WormAILevel, "AIParams.Worm08")
        CopyContainer(WormAILevel, "AIParams.Worm09")         
        CopyContainer(WormAILevel, "AIParams.Worm10")
        CopyContainer(WormAILevel, "AIParams.Worm11")  
        
	lib_SetupTeamInventory(0, "Inv_Player")
	lib_SetupTeamInventory(1, "Inv_Enemy")
    
    
    lib_SetupTeamWeaponDelays(0, "PlayerDelays")
    lib_SetupTeamWeaponDelays(1, "EnemyDelays")

    PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
    StartFirstTurn()
end


function DoOncePerTurnFunctions()

    SendMessage("GameLogic.DropRandomCrate")

end



function TurnEnded()
	lib_DeathmatchChallengeTurnEnded()
end
