
function Initialise()						

    SetData("RoundTime", 3000000)
    
    lib_SpawnCrate("Crate1_Shotgun")
	
    lib_SetupTeam(0, "PlayersTeam")
    lib_SetupTeam(1, "CPUTeam")

    lib_SetupWorm(0, "PlayersWorm")  
    lib_SetupWorm(1, "CPUWorm1")  
    lib_SetupWorm(2, "CPUWorm2")  
    lib_SetupWorm(3, "CPUWorm3")  
    lib_SetupWorm(4, "CPUWorm4")  
  
    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU1" 
    
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02")
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04")
    
    lib_SetupWormInventory(1, "CPU1_Inv")
    lib_SetupWormInventory(2, "CPU2_Inv")
    lib_SetupWormInventory(3, "CPU3_Inv")
    lib_SetupWormInventory(4, "CPU4_Inv")
    
    
    lib_SetupTeamInventory(0, "PlayersInventory")
    
    RoundTimeRemaining = 3000000
    
    Crate1ShotgunCollected = true
    Crate2UziCollected = true
    Crate3BazookaCollected = true
    Crate4GrenadeCollected = true
    
    EnemyWormsDead = 0
    PlayerHasDied = false
--    DoNotDoThis = false  
    g_nNextCrate = 2
    NumberOfCratesOnMap = 1
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)     
    
  
    PlayIntroMovie()
end

function SetWind()
    
    local MaxWind = 0.000170
    SetData("Wind.Speed",0.2*MaxWind)
    SetData("Wind.Direction",5.5)
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    StartFirstTurn()
end



function Worm_Died()

    local DeadWorm = GetData("DeadWorm.Id")
    
    if DeadWorm == 0  then
    
        PlayerHasDied = true
        
    elseif DeadWorm > 0 then
        

        EnemyWormsDead = EnemyWormsDead + 1
    end
        
end


function TurnEnded()

    RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
 
    elseif PlayerHasDied == true then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    elseif EnemyWormsDead == 4 then 
    
        SetData("EFMV.GameOverMovie", "Outro") -- the enemy has been defeated so
        SendMessage("GameLogic.Mission.Success") -- the mission is a success

    
    else
        
        StartTurn()
    end
end


function Crate_Collected()
    
    
    CrateIndex = GetData("Crate.Index")
    
    -- each time a crate is collected set the variable to true.
    if CrateIndex == 1 then
    
        Crate1ShotgunCollected = true
    
    elseif CrateIndex == 2 then
        
        Crate2UziCollected = true
        
    elseif CrateIndex == 3 then
    
        Crate3BazookaCollected = true
        
    elseif CrateIndex == 4 then
    
        Crate4GrenadeCollected = true
    end
end


function SpawnNextCrate()
        
    if Crate1ShotgunCollected == true then
       
        lib_SpawnCrate("Crate1_Shotgun")
        Crate1ShotgunCollected = false
        
    elseif Crate2UziCollected == true then
    
        lib_SpawnCrate("Crate2_Uzi")
        Crate2UziCollected = false
        
    elseif Crate3BazookaCollected == true then
        
        lib_SpawnCrate("Crate3_Bazooka")
        Crate3BazookaCollected = false
        
    elseif Crate4GrenadeCollected == true then
        
        lib_SpawnCrate("Crate4_Grenade")
        Crate4GrenadeCollected = false
    end   
end


function DoOncePerTurnFunctions()
    

    if PlayerHasDied == false and RoundTimeRemaining > 0 then
        
        SpawnNextCrate()
    end

end


