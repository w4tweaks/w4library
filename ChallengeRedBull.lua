function Initialise()
	
    SetData("HUD.Counter.Active", 1) 
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
	SetData("HUD.Clock.DisplayTenths", 1)
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupWorm(0, "Player")
	lib_SetupTeamInventory(0, "Inv_Human")
	
	
	SendMessage("WormManager.Reinitialise")
	
	SetData("Trigger.Visibility", 1)

    TargetsLeft = 8
    g_nNextTarget = 1
    SendMessage("Commentary.NoDefault")
	
    SetHudCounter()
    SpawnNextTarget()
   -- StartFirstTurn()
   PlayIntroMovie()
   
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function Crate_Collected()
    
    TargetsLeft = TargetsLeft - 1
    SetData("HUD.Counter.Value", TargetsLeft) 
    
    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
        local myRandomInteger = lib_GetRandom(1, 4)
        
        lib_Comment(TextToDisplay[myRandomInteger])
    
    
    SpawnNextTarget()
   
end

function SetHudCounter()

    SetData("HUD.Counter.Value", TargetsLeft) 
end


function SpawnNextTarget()

    if g_nNextTarget == 9 then
    
        lib_DisplaySuccessComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
        
    else
    
    -- target cycle 
    local Crate = {"Targ1", "Targ2","Targ3", "Targ4", "Targ5", "Targ6", 
    				"Targ7", "Targ8"}
                    
        lib_SpawnCrate(Crate[g_nNextTarget])
        g_nNextTarget = g_nNextTarget + 1
        
    log("g_nNextTarget = ", g_nNextTarget)

    end

end


function TurnEnded()
        StartTurn()
    
end


function Worm_Died()
    
    deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Failure")
    end
end
