function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Player")
	lib_SetupWorm(0, "kWORM0")
        lib_SetupWorm(1, "kWORM1")

	SendMessage("WormManager.Reinitialise")
	
    
        PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
