
-- Tutorial 1
--
-- For me to do:

-- Waiting on code requests:

-- Waiting on art requests:

-- Helpful:

-- Do a search for "DEBUG" comments, these should be removed in a release style build.

function Initialise()
	SetData("VictoryEndsTurn",0)    -- This flag stops the shotgun ending on the first shot cos it thinks there is no enemy left to shoot 
    kDialogueBoxTimeDelay = 0500    -- This is the delay between selecting a weapon and having the game display a dialogue box.
                                    -- Originally: 0200
    kTurnOpeningTimeDelay = 0700    -- The delay before the first text box opens when the turn starts in Game2 and Game 3
    kVariables()   

    SendMessage("Commentary.NoDefault")

    SetData("Mine.DetonationType", 1)
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
    SendMessage("GameLogic.PlaceObjects")

	SetData("HotSeatTime", 0)
    SetData("RoundTime",-1)
    SetData("TurnTime",0)

    lib_SetupTeam(0, "Team_Human")
    lib_SetupWorm(0, "Worm.Human0")

    lib_SetupTeam(1, "Team_Enemy")
    lib_SetupWorm(4, "Worm.Enemy0")
    lib_SetupWorm(5, "Worm.Enemy1")
    lib_SetupWorm(6, "Worm.Enemy2")
    lib_SetupWorm(7, "Worm.Enemy3")

    lib_SetupTeam(2, "Team_Actors")
    lib_SetupWorm(10, "Actor.Worm10.HumanDummy")
    lib_SetupWorm(11, "Actor.Worm11.Conductor")
    lib_SetupWorm(12, "Actor.Worm12.Passenger2")
    lib_SetupWorm(13, "Actor.Worm13.Passenger1")
    lib_SetupWorm(14, "Actor.Worm14.Driver")
    lib_SetupWorm(15, "Actor.Worm15.Worminkle")

    lib_SetupTeam(3, "Team_Human_Inactive")
    lib_SetupWorm(1, "Worm.Human1")
    lib_SetupWorm(2, "Worm.Human2")
    lib_SetupWorm(3, "Worm.Human3")

    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(0, "Inventory_Human")
    lib_SetupTeamInventory(1, "Inventory_Enemy")
    lib_SetupTeamInventory(2, "Inventory_Enemy")
    
    -- Stop human team from dropping Gravestones.
    lock, team_data = EditContainer("Team.Data00")
    team_data.GraveIndex = 255
    CloseContainer(lock)

    -- Stop enemy team from dropping Gravestones.
    lock, team_data = EditContainer("Team.Data01")
    team_data.GraveIndex = 255
    CloseContainer(lock)

    SetData("Trigger.Visibility", 0)

    SetData("Land.Indestructable", 1)

    SetData("HUD.Energy.Visible.Team03", 0)
    
    SetData("Wind.Speed", 0)  -- no wind


    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HelpPanelDelay = 0          
    CloseContainer(lock)
	
    -- Kick the game off by running the first little EFMV snippet.
    SetData("EFMV.Unskipable", 0)
    kPlayEFMV("EFMV.Intro")
end

function EFMV_Terminated()
	local WhichMovie = GetData("EFMV.MovieName")
    if WhichMovie == "EFMV.Intro" then
        Create_Crates_Wave1()
        SetData("EFMV.Unskipable", 1)
        kPlayEFMV("EFMV.Intro_Dialogue")
    elseif WhichMovie == "EFMV.Intro_Dialogue" then
        SetData("EFMV.Unskipable", 0)
        AfterEFMVEndsSetUpGameWormSpawns()
    elseif WhichMovie == "EFMV.Success_Best"
    or WhichMovie == "EFMV.Success_OK"
    or WhichMovie == "EFMV.Success_Worst" then
        kSendSuccessMessage()
    elseif WhichMovie == "EFMV.Failure" then
        --unspawn worminkle
        SetData("EFMV.Unskipable", 0)
        SendIntMessage("WXWormManager.UnspawnWorm", 15)
        StartTurn()
    elseif WhichMovie == "EFMV.Crates_Wave1_LI" then
        kPlayEFMV("EFMV.Crates_Wave1_END")
    elseif WhichMovie == "EFMV.Crates_Wave2_LI" then
        kPlayEFMV("EFMV.Crates_Wave2_END")
    elseif WhichMovie == "EFMV.TurnEnded1_LI" then
        kPlayEFMV("EFMV.TurnEnded1_DB")
    elseif WhichMovie == "EFMV.TurnEnded2_LI" then
        kPlayEFMV("EFMV.TurnEnded2_DB")
    elseif WhichMovie == "EFMV.TurnEnded3_LI" then
        kPlayEFMV("EFMV.TurnEnded3_DB")
    elseif WhichMovie == "EFMV.TurnEnded1_DB"
    or WhichMovie == "EFMV.TurnEnded2_DB" 
    or WhichMovie == "EFMV.TurnEnded3_DB" then
        kDontBeginTurn = false
        StartTimer("StartTurn", 1000)
    end
end

function AfterEFMVEndsSetUpGameWormSpawns()
    SendIntMessage("WXWormManager.UnspawnWorm", 10)
    SendIntMessage("WXWormManager.UnspawnWorm", 11)
    SendIntMessage("WXWormManager.UnspawnWorm", 12)
    SendIntMessage("WXWormManager.UnspawnWorm", 13)
    SendIntMessage("WXWormManager.UnspawnWorm", 14)
    SendIntMessage("WXWormManager.UnspawnWorm", 15)

    Create_Bazooka_Triggers()
    --Create_Grenade_Triggers()
    --Create_AutoHop_Triggers()
    
    DrunkPart1 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm1")
    DrunkPart2 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm2")
    DrunkPart3 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm3")
    DrunkPart4 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm4")
    
    StartTimer("RespawnBubbleEmitters", 50000)
    
    StartFirstTurn()
end

function RespawnBubbleEmitters()
    if kWorm1Damaged == false then
        lib_DeleteEmitterImmediate(DrunkPart3)
        DrunkPart3 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm3")
    end
    
    if kWorm2Damaged == false then
        lib_DeleteEmitterImmediate(DrunkPart4)
        DrunkPart4 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm4")
    end
    
    if kWorm3Damaged == false then
        lib_DeleteEmitterImmediate(DrunkPart1)
        DrunkPart1 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm1")
    end
    
    if kWorm4Damaged == false then
        lib_DeleteEmitterImmediate(DrunkPart2)
        DrunkPart2 = lib_CreateEmitter("WXPL_DrunkenBubbles", "Part.DrunkWorm2")
    end
    
    StartTimer("RespawnBubbleEmitters", 50000)
end

function Game_BriefingDialogNowOff()
    if g_BoxId == "T1_R.EnemyDamaged" or g_BoxId == "T1_R.PlayerDamaged" then
        IfFirstTurnHasEndedPlayDrunkMovie()
        
        if kDontStartCrateSequence == true then
            kDontStartCrateSequence = false
            if kNumberOfCratesCollected == 4 then
                StartTimer("CheckIventoryToSpawnNewCrates", 0500)
            end
        end
    end
end

function Trigger_Collected()
	TriggerIndex = GetData("Trigger.Index")
    
	if TriggerIndex == 1 then
        kDeleteTrigger(1)
        kGrenadeTriggerCollected = true
        if kBazookaTriggerCollected == true then
            kMakeBriefingBox("T1_R.Jump_Hop_Second")
        elseif kBazookaTriggerCollected == false then
            kMakeBriefingBox("T1_R.Jump_Hop_First")
        end
    elseif TriggerIndex == 2 then
        kDeleteTrigger(2)
        kBazookaTriggerCollected = true
		kMakeBriefingBox("T1_R.Jump_Flip_Second")
    elseif TriggerIndex == 3 then
        kDeleteTrigger(3)
        kMakeBriefingBox("T1_R.AutoHop")
    end
end

function TurnStarted()
    SetData("Wind.Speed", 0)

    TurnCounter = TurnCounter + 1

    if kThisIsFirstDMTurn == true then
        kPlayEFMV("PositionCameraAtStartOfTurn")
        StartTimer("Delay_FirstTurn_DialogueBox", 1000)
        kThisIsFirstDMTurn = false
    end
    
    if kWhichWormHasTakenDamage == 0 then
        IfFirstTurnHasEndedPlayDrunkMovie()
    elseif kWhichWormHasTakenDamage == 1 then
        -- Human player was the first worm to take damage
        kWhichWormHasTakenDamage = 10
        kMakeBriefingBox("T1_R.PlayerDamaged")
        kDontStartCrateSequence = true
    elseif kWhichWormHasTakenDamage == 2 then
        -- Enemy was the first worm to take damage
        kWhichWormHasTakenDamage = 10
        kMakeBriefingBox("T1_R.EnemyDamaged")
        kDontStartCrateSequence = true
    end
    
--~     if kNumberOfCratesCollected == 4 and kDontStartCrateSequence == false then
--~         CheckIventoryToSpawnNewCrates()
--~     end
end

--~ function CheckIventoryToSpawnNewCrates()
--~     allianceInventory = QueryContainer("Inventory.Alliance00")
--~     if allianceInventory.Bazooka == 0
--~     and allianceInventory.Grenade == 0
--~     and allianceInventory.Shotgun == 0
--~     and allianceInventory.FirePunch == 0 then
--~         kNumberOfCratesCollected = 0
--~         if kNextCrateCycle == 1 then
--~             kNextCrateCycle = 2
--~             Create_Crates_Wave1()
--~             kPlayEFMV("EFMV.Crates_Wave1_LI")
--~         elseif kNextCrateCycle == 2 then
--~             kNextCrateCycle = 1
--~             Create_Crates_Wave2()
--~             kPlayEFMV("EFMV.Crates_Wave2_LI")
--~         end
--~     end
--~ end

function IfFirstTurnHasEndedPlayDrunkMovie()
    if TurnCounter > 1 then
        if kAlreadyPlayedEndOfTurnDrunkAnim == false then
            kAlreadyPlayedEndOfTurnDrunkAnim = true
            if kWorm4Damaged == false then
                kDontBeginTurn = true
                kPlayEFMV("EFMV.TurnEnded1_LI")
            elseif kWorm2Damaged == false then
                kDontBeginTurn = true
                kPlayEFMV("EFMV.TurnEnded2_LI")
            else
                kDontBeginTurn = true
                kPlayEFMV("EFMV.TurnEnded3_LI")
            end
        end
    end
end

function Delay_FirstTurn_DialogueBox()
    kMakeBriefingBox("T1_R.Opening1")
end

function TurnEnded()
    if kTimeToActivateWorm == true then
        if kActivateNextWorm == 1 then
            kUnspawnAWormQuietly(kActivateNextWorm)
            lib_SetupWorm(kActivateNextWorm, "Worm.Human1_Activate")
            SendIntMessage("Worm.Respawn", kActivateNextWorm)
        elseif kActivateNextWorm == 2 then
            kUnspawnAWormQuietly(kActivateNextWorm)
            lib_SetupWorm(kActivateNextWorm, "Worm.Human2_Activate")
            SendIntMessage("Worm.Respawn", kActivateNextWorm)
        elseif kActivateNextWorm == 3 then
            kUnspawnAWormQuietly(kActivateNextWorm)
            lib_SetupWorm(kActivateNextWorm, "Worm.Human3_Activate")
            SendIntMessage("Worm.Respawn", kActivateNextWorm)
        end
        
        kTimeToActivateWorm = false
        kActivateNextWorm = 10
    end
    

    if kNumberOfEnemyAlive == 0 and kMissionSuccess == false then
        kTutorialOutro()
    else
        if kNumberOfHumanAlive == 0 and kMissionSuccess == false then
            lib_SetupWorm(0, "Worm.Human0")
            lib_SetupWorm(1, "Worm.Human1")
            lib_SetupWorm(2, "Worm.Human2")
            lib_SetupWorm(3, "Worm.Human3")
            SendIntMessage("Worm.Respawn", 0)
            SendIntMessage("Worm.Respawn", 1)
            SendIntMessage("Worm.Respawn", 2)
            SendIntMessage("Worm.Respawn", 3)
            
            kNumberOfHumanAlive = 4
            
            lib_SetupWorm(15, "Actor.Worm15.Worminkle")
            SendIntMessage("Worm.Respawn", 15)
            
            kActivateNextWorm = 10
            kTimeToActivateWorm = false
        
            SetData("EFMV.Unskipable", 1)
            kPlayEFMV("EFMV.Failure")
        else
            if kDontBeginTurn == false then
                StartTurn()
            end
        end
    end
end

--~ function Weapon_Selected()
--~ 	local worm = lib_QueryWormContainer()
--~     
--~     kWormTeamIndex = worm.TeamIndex
--~     kWormWeaponIndex = worm.WeaponIndex
--~     
--~     if worm.TeamIndex == 1 then return end
--~     
--~     if worm.WeaponIndex == "WeaponBazooka" and kBazooka_HasAlreadyBeenSelected == false then
--~         kPlayEFMV("Hud.AimingPower")
--~         kBazooka_HasAlreadyBeenSelected = true
--~         StartTimer("Bazooka_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponGrenade" and kGrenade_HasAlreadyBeenSelected == false then
--~         kPlayEFMV("Hud.AimingPower")
--~         kGrenade_HasAlreadyBeenSelected = true
--~         StartTimer("Grenade_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponShotgun" and kShotgun_HasAlreadyBeenSelected == false then
--~         kShotgun_HasAlreadyBeenSelected = true
--~         StartTimer("Shotgun_DBox", kDialogueBoxTimeDelay)
--~     elseif worm.WeaponIndex == "WeaponFirePunch" and kFirePunch_HasAlreadyBeenSelected == false then
--~         kFirePunch_HasAlreadyBeenSelected = true
--~         StartTimer("Firepunch_DBox", kDialogueBoxTimeDelay)
--~     end
--~ end

function Bazooka_DBox()
    if kAimingHasBeenExplained == false then
        kAimingHasBeenExplained = true
        kMakeBriefingBox("T1_R.BazookaFirst")
    elseif kAimingHasBeenExplained == true then
        kMakeBriefingBox("T1.Game3.Bazooka")
    end
end

function Grenade_DBox()
    if kAimingHasBeenExplained == false then
        kAimingHasBeenExplained = true
        kMakeBriefingBox("T1_R.GrenadeFirst")
    elseif kAimingHasBeenExplained == true then
        kMakeBriefingBox("T1_R.Grenade")
    end
end

function Shotgun_DBox()
    kMakeBriefingBox("T1.Game3.Shotgun")
end

function Firepunch_DBox()
    kMakeBriefingBox("T1_R.Firepunch")
end

function Worm_Died()
    if kWeAreUnspawningWorms == false then
        deadworm = GetData("DeadWorm.Id")
        if deadworm == 0
        or deadworm == 1
        or deadworm == 2
        or deadworm == 3 then
            if kPlayerDiesForFirstTime == false then
                kPlayerDiesForFirstTime = true
                kMakeBriefingBox("T1.Game3.FirstPlayerDead")
            end
        
            kNumberOfHumanAlive = kNumberOfHumanAlive - 1
            kActivateNextWorm = deadworm + 1
            kTimeToActivateWorm = true
            
            if kNumberOfHumanAlive == 1 and kPlayerDownToLastWorm == false then
                kPlayerDownToLastWorm = true
                kMakeBriefingBox("T1.Game3.LastPlayerWorm")
            end  
        elseif deadworm == 4
        or deadworm == 5
        or deadworm == 6
        or deadworm == 7 then
            kNumberOfEnemyAlive = kNumberOfEnemyAlive - 1
        
            -- added to ensure game over if last enemy killed by first shot of shotgun - bug 493 (MS)
            if kNumberOfEnemyAlive == 0 then
                kTutorialOutro()
                return
            end

            if kEnemyDiesForFirstTime == false then
                kEnemyDiesForFirstTime = true
                kMakeBriefingBox("T1.Game3.FirstEnemyDead")
            end

            if kNumberOfEnemyAlive == 1 and kEnemyDownToLastWorm == false then
                kEnemyDownToLastWorm = true
                kMakeBriefingBox("T1.Game3.LastEnemyWorm")
            end
        end
    end
end

function Worm_Damaged()
    local DamagedWormId = GetData("DamagedWorm.Id") -- a number 0-15
    local DamageTypeTaken = GetData("DamageTypeTaken") -- 0-5 (see below)

    if DamagedWormId == 0
    or DamagedWormId == 1
    or DamagedWormId == 2
    or DamagedWormId == 3 then
        if kWhichWormHasTakenDamage < 10 then
            kWhichWormHasTakenDamage = 1
        end
    elseif DamagedWormId == 4 then
        lib_DeleteEmitterImmediate(DrunkPart3)
        kWorm1Damaged = true
        HasEnemyWormHasTakenDamageFirst()
    elseif DamagedWormId == 5 then
        lib_DeleteEmitterImmediate(DrunkPart4)
        kWorm2Damaged = true
        HasEnemyWormHasTakenDamageFirst()
    elseif DamagedWormId == 6 then
        lib_DeleteEmitterImmediate(DrunkPart1)
        kWorm3Damaged = true
        HasEnemyWormHasTakenDamageFirst()
    elseif DamagedWormId == 7 then
        lib_DeleteEmitterImmediate(DrunkPart2)
        kWorm4Damaged = true
        HasEnemyWormHasTakenDamageFirst()
    end
end

function HasEnemyWormHasTakenDamageFirst()
    if kWhichWormHasTakenDamage < 10 then
        kWhichWormHasTakenDamage = 2
    end
end

function Crate_Collected()
    if kThisIsTheFirstCrateCollected == true then
        kThisIsTheFirstCrateCollected = false
        kMakeBriefingBox("T1_R.CollectFirstCrate")
    end
   
	CrateNumber = GetData("Crate.Index")
    if CrateNumber == 10 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponBazooka")
    elseif CrateNumber == 20 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponFirePunch")
    elseif CrateNumber == 30 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponGrenade")
    elseif CrateNumber == 40 then
        kNumberOfCratesCollected = kNumberOfCratesCollected + 1
        lib_Comment("Text.kWeaponShotgun")
    end
    
    if kNumberOfCratesCollected == 3 and kScannerHasBeenExplained == false then
        kScannerHasBeenExplained = true
        kPlayEFMV("Hud.Scanner")
        StartTimer("Scanner_DBox", 0500)
    end
end

function Scanner_DBox()
    kMakeBriefingBox("T1_R.Scanner")
end

-- ***************************************************
-- **                VARIABLE ROOM                  **
-- ***************************************************

function kVariables()
    kEnemyDiesForFirstTime = false                      -- is set to true the first time a worm from either team dies
    kPlayerDiesForFirstTime = false

    kEnemyDownToLastWorm = false                        -- is set to true when either team is down to their last worm
    kPlayerDownToLastWorm = false
    kCurrentWeaponFired = "nil"                         -- The name of the current weapon being fired is copied into this variable.
    kLastWeaponFired = "nil"                            -- The name of the last weapon that was fired is copied into this variable.
    kDisplayedSameWeaponMessageAlready = false          -- This variable counts how many times the user has fired the same weapon.
    kPlayFailureEFMV = false                            -- Set to true when all human worms die.
    
    -- here downward is all new variables, above variables gonna be choppes!
    
    kTimeToActivateWorm = false                         -- Set to true when about the spawn a new human player worm in. (see "function TurnEnded")
    kActivateNextWorm = 10                              -- This index no. tells me which human player worm to spawn in next (after one dies).
    kWeAreUnspawningWorms = false                       -- Set to true when unspawning worms because otherwise "function Worm_Died" picks up on it.
    kThisIsTheFirstCrateCollected = true                -- Controls whether or not dialogue box is displayed when crate is collected.
    kGrenadeTriggerCollected = false                    -- Set to true if Grenade trigger is collected.
    kBazookaTriggerCollected = false                    -- Set to true if Grenade trigger is collected.
    kNumberOfCratesCollected = 0                        -- counts how many crates the player has collected
    kNextCrateCycle = 2                                 -- what wave of crates should be spawned in next, either 1 or 2
    kWorm1Damaged = false                               -- This tells me which of the enemy worms have taken a hit. If they haven't yet taken
    kWorm2Damaged = false                               -- a hit, then we respawn their drunk bubble particle when it expires.
    kWorm3Damaged = false
    kWorm4Damaged = false
    kWhichWormHasTakenDamage = 0                        -- tracks whether or not enemy or human was the first worm to take damage
    kDontStartCrateSequence = false                     -- used to make sure new crate EFMV and damage dialogue boxes dont collide
    kDontBeginTurn = false                              -- If this is true the next turn cannot begin.
    TurnCounter = 0                                     -- increment every time in turn started function
    kAlreadyPlayedEndOfTurnDrunkAnim = false
    
    -- here downward is all old variables being reused
    
    kThisIsFirstDMTurn = true                           -- tells me if this is the first turn
    kBazooka_HasAlreadyBeenSelected = false             -- these variables tell me which weapons have been used and which haven't
    kGrenade_HasAlreadyBeenSelected = false             
    kShotgun_HasAlreadyBeenSelected = false
    kFirePunch_HasAlreadyBeenSelected = false
    kSkipGo_HasAlreadyBeenSelected = false
    
    kNumberOfEnemyAlive = 4                             -- counts how many enemy are alive on the landscape
    kNumberOfHumanAlive = 4                             -- counts how many human worms are alive
    
    kAimingHasBeenExplained = false                     -- set to true when a weapon making a dialogue box explaining aiming has been selected
    kPowerHasBeenExplained = false                      -- set to true when a weapon making a dialogue box explaining power bar has been selected
    
    kScannerHasBeenExplained = false                    -- set to true as soon as the scanner is explained
    
    kMissionSuccess = false                             -- set to true when the mission is completed
end

-- ***************************************************
-- **                 ENGINE ROOM                   **
-- ***************************************************

function kDeleteTrigger(kWhichTrigger)
    SendIntMessage("GameLogic.DestroyTrigger",kWhichTrigger)
end

function kMakeBriefingBox(kSayWhat)
    g_BoxId = kSayWhat
    --lib_CreateBriefingBox(g_BoxId)
    
    lib_CreateWXBriefingBox("WXFEP.InGameBriefMiddle",g_BoxId,"FE.FramedGfx:n")
end

function kPlayEFMV(kWhatIsEFMVCalled)
    SetData("EFMV.MovieName", kWhatIsEFMVCalled)
    SendMessage("EFMV.Play") 
end

function kTutorialOutro()
    kMissionSuccess = true

    lib_SetupWorm(15, "Actor.Worm15.Worminkle")
    SendIntMessage("Worm.Respawn", 15)
    
    if kNumberOfHumanAlive == 4 then
        SetData("EFMV.Unskipable", 1)
        SetData("EFMV.GameOverMovie", "EFMV.Success_Best")
        kSendSuccessMessage()
    elseif kNumberOfHumanAlive == 3 
    or kNumberOfHumanAlive == 2 then
        SetData("EFMV.Unskipable", 1)
        SetData("EFMV.GameOverMovie", "EFMV.Success_OK")
        kSendSuccessMessage()
    elseif kNumberOfHumanAlive == 1
    or kNumberOfHumanAlive == 0 then
        SetData("EFMV.Unskipable", 1)
        SetData("EFMV.GameOverMovie", "EFMV.Success_Worst")
        kSendSuccessMessage()
    end
end

function kTutorialFailure()
end

function kSendSuccessMessage()
    SendMessage("GameLogic.Mission.Success")
end

function kUnspawnAWormQuietly(kUnspawnWhichWorm)
    kWeAreUnspawningWorms = true
    SendIntMessage("WXWormManager.UnspawnWorm", kUnspawnWhichWorm)
    kWeAreUnspawningWorms = false
end

-- ***************************************************
-- **                 TRIGGER ROOM                  **
-- ***************************************************

function Create_Bazooka_Triggers()
    lib_SpawnTrigger("Trigger.Bazooka1")
    lib_SpawnTrigger("Trigger.Bazooka2")
    lib_SpawnTrigger("Trigger.Bazooka3")
    lib_SpawnTrigger("Trigger.Bazooka4")
end

function Create_Grenade_Triggers()
    lib_SpawnTrigger("Trigger.Grenade1")
    lib_SpawnTrigger("Trigger.Grenade2")
    lib_SpawnTrigger("Trigger.Grenade3")
end

function Create_AutoHop_Triggers()
    lib_SpawnTrigger("Trigger.AutoHop1")
    lib_SpawnTrigger("Trigger.AutoHop2")
    lib_SpawnTrigger("Trigger.AutoHop3")
    lib_SpawnTrigger("Trigger.AutoHop4")
    lib_SpawnTrigger("Trigger.AutoHop5")
    lib_SpawnTrigger("Trigger.AutoHop6")
    lib_SpawnTrigger("Trigger.AutoHop7")
end

-- ***************************************************
-- **                     CRATES                    **
-- ***************************************************

function Create_Crates_Wave1()
    lib_SpawnCrate("WeaponCrate.Bazooka")
    lib_SpawnCrate("WeaponCrate.Grenade")
    lib_SpawnCrate("WeaponCrate.Firepunch")
    lib_SpawnCrate("WeaponCrate.Shotgun")
end

function Create_Crates_Wave2()
    lib_SpawnCrate("WeaponCrate.Bazooka_2")
    lib_SpawnCrate("WeaponCrate.Grenade_2")
    lib_SpawnCrate("WeaponCrate.Firepunch_2")
    lib_SpawnCrate("WeaponCrate.Shotgun_2")
end
