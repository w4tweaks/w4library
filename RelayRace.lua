
    
function Initialise()

    -- 3 worms each with 100 health, not allied
    lib_QuickSetupMultiplayer(3, 100, false)

    lib_SetupTeamInventory(0, "Inventory")
    lib_SetupTeamInventory(1, "Inventory")
    lib_SetupTeamInventory(2, "Inventory")
    lib_SetupTeamInventory(3, "Inventory")

    SetData("RoundTime", 0 ) -- makes the round counter count up
    SetData("TurnTime", 0 )
    
    SetData("Trigger.Visibility", 1)
    
    lib_SpawnCrate("Crate0")
    lib_SpawnCrate("Crate1")
    lib_SpawnCrate("Crate2")
    lib_SpawnCrate("Crate3")
    
    lib_SpawnTrigger("T0_Checkpoint0")
    lib_SpawnTrigger("T1_Checkpoint0")
    lib_SpawnTrigger("T2_Checkpoint0")
    lib_SpawnTrigger("T3_Checkpoint0")

    lib_SpawnTrigger("T0_Checkpoint1")
    lib_SpawnTrigger("T1_Checkpoint1")
    lib_SpawnTrigger("T2_Checkpoint1")
    lib_SpawnTrigger("T3_Checkpoint1")

    lib_SpawnTrigger("T0_Finish")
    lib_SpawnTrigger("T1_Finish")
    lib_SpawnTrigger("T2_Finish")
    lib_SpawnTrigger("T3_Finish")

    -- cumulative time for each team
    g_Time = {0,0,0,0} 
    
    --SendMessage("Game.PlaceObjects")
    StartFirstTurn()

end

function Trigger_Collected()
    local TriggerIndex = GetData("Trigger.Index")

    -- Find out which team's time to update depending on the trigger
    if TriggerIndex == 0 or TriggerIndex == 1 or TriggerIndex == 2 then
        team = 0
    elseif TriggerIndex == 3 or TriggerIndex == 4 or TriggerIndex == 5 then
        team = 1
    elseif TriggerIndex == 6 or TriggerIndex == 7 or TriggerIndex == 8 then
        team = 2
    elseif TriggerIndex == 9 or TriggerIndex == 10 or TriggerIndex == 11 then
        team = 3
    end

    local Time = GetData("ElapsedRoundTime")
    g_Time[team+1] = Time

    -- its all over if the last worm finishes
    SendMessage("GameLogic.GetAllTeamsHadTurn")
    local AllTeamsHadTurn = GetData("GameLogic.AllTeamsHadTurn")
    if AllTeamsHadTurn==1 and (
            TriggerIndex==2 or
            TriggerIndex==5 or
            TriggerIndex==8 or
            TriggerIndex==11)
    then
        AwardWin()
    else
        Timer_TurnTimedOut() -- need weapon delete and end turn
    end
end

function TurnStarted()
    -- Put the clock back to this teams accumulated time so far
    local worm = lib_QueryWormContainer()
    SetData("ElapsedRoundTime", g_Time[worm.TeamIndex+1])
end

function TurnEnded()
    -- No default deathmath rules here
    SendMessage("WormManager.GetActiveAlliances")
    AllianceCount = GetData("AllianceCount")
    if AllianceCount == 0 then
       -- all the teams have just snuffed it/surrendered
       SendMessage("GameLogic.Draw")
    else 
       StartTurn()
    end
end

function Worm_Died()
    
    local Id = GetData("DeadWorm.Id")
    local worm = lib_QueryWormContainer(Id)
    g_Time[worm.TeamIndex+1] = -1 -- no score

    SendMessage("Team.Surrender") -- team cannot play on

end


function AwardWin()
    local WinningTeam = -1
    local WinningTime = -1
    local n
    for n = 1,4 do
       if g_Time[n] ~= -1 and (g_Time[n] < WinningTime or WinningTime==-1) then
           WinningTime = g_Time[n]
           WinningTeam = n
       end
    end

    SendIntMessage("GameLogic.Win", WinningTeam-1)

end
