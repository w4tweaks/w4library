
    
function Initialise()

    -- 4 worms each with 100 health, allied
    lib_QuickSetupMultiplayer(4, 100, true)

    lib_SetupTeamInventory(0, "Inventory")
    lib_SetupTeamInventory(1, "Inventory")
    lib_SetupTeamInventory(2, "Inventory")
    lib_SetupTeamInventory(3, "Inventory")

    -- Set all random crates to be health crates
    -- for random health drop
    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HealthChance = 1
    scheme.WeaponChance = 0
    scheme.UtilityChance = 0
    CloseContainer(lock)

    StartFirstTurn()

end

function TurnEnded()

    -- counts up the number of land blocks under the land trees
    -- named CODE:AAAA etc
    local i
    local Code = {"AAAA", "BBBB", "CCCC", "DDDD"}
    local Land = 0
    for i = 1,4 do
       SendStringMessage("Land.GetLandRemaining", Code[i] )
       Land = Land + GetData("Land.LandRemaining")
    end
   
    local kMinLand = 10
    if Land < kMinLand then
       SendIntMessage("GameLogic.Win", 1)
    else
        CheckOneTeamVictory() -- standard deathmatch rules
    end   
end


function DoOncePerTurnFunctions()
   SendMessage("GameLogic.DropRandomCrate")
end
