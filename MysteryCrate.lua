-- Mystery Crates Testing
function Initialise()
    -- Sets up mine fuse time and probability
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 3000)
	SetData("Mine.MaxFuse", 3000)

    -- Create mines and oildrums 
    SendMessage("GameLogic.PlaceObjects")

    --spawn crates
    lib_SpawnCrate("MineTriplet")
    lib_SpawnCrate("MineLayer")
    lib_SpawnCrate("MineTriplet")
    lib_SpawnCrate("BarrelTriplet")
    lib_SpawnCrate("Flood")
    lib_SpawnCrate("Disarm")
    lib_SpawnCrate("Teleport")
    lib_SpawnCrate("QuickWalk")
    lib_SpawnCrate("Health")
    lib_SpawnCrate("DoubleTurnTime")
    lib_SpawnCrate("Damage")
    
    SetupWormsAndTeams()
    SetupInventories()

    SetData("Camera.StartOfTurnCamera", "Default")
    StartFirstTurn()
end

function SetupWormsAndTeams()
-- Activate Team 0
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = "Team 1"
    team.TeamColour = 0
-- team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Activate Team 1
    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "Team 2"
    team.TeamColour = 1
-- team.IsAIControlled = true
    CloseContainer(lock)

-- Worm 0, Team 0
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = "T1,W1"
    worm.Energy = 100
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    worm.SfxBankName = "Pirate"
    worm.Spawn = "spawn"
    CloseContainer(lock) 

-- Worm 4, Team 1
    CopyContainer("Worm.Data00", "Worm.Data04")
    lock, worm = EditContainer("Worm.Data04")
    worm.Energy = 80 
    worm.Name = "T2,W1"
    worm.TeamIndex = 1
    worm.SfxBankName = "Biggles"
    worm.Spawn = "spawn"
    CloseContainer(lock)


   SendMessage("WormManager.Reinitialise")
end
function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
    
	inventory.Bazooka = -1
    inventory.Grenade = -1
    inventory.ClusterGrenade =-1
    inventory.Airstrike = -1
    inventory.Dynamite = -1
    inventory.HolyHandGrenade = -1
    inventory.GasCanister = -1
    inventory.BananaBomb = -1
    inventory.Landmine = -1
    inventory.HomingMissile = -1
    inventory.Sheep = -1
    inventory.SuperSheep = -1
    inventory.Parachute = -1
    inventory.Jetpack = -1
    inventory.SkipGo = -1
    inventory.OldWoman = -1
    inventory.Girder = -1
    inventory.BridgeKit = -1
    inventory.Shotgun = -1
    inventory.GasCanister = -1
    inventory.NinjaRope = -1
    inventory.FirePunch = -1
    inventory.Prod = -1
    inventory.ConcreteDonkey = -1
    inventory.BaseballBat = -1
    inventory.Flood = -1
    inventory.Redbull = -1
    inventory.WeaponFactoryWeapon = -1
    inventory.Starburst = -1
    inventory.ChangeWorm = -1
    inventory.Surrender = -1
    inventory.SentryGun = -1
    inventory.Fatkins = -1
    inventory.PoisonArrow = -1
    inventory.Scouser = -1
    inventory.BubbleTrouble = -1
    
    CloseContainer(lock) -- must close the container ASAP

    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)

-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Alliance00")
   CopyContainer("Inventory.Team.Default", "Inventory.Alliance01")
end