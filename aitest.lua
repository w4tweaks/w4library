


-- Mission Basic setup 1v1 player

function Initialise()

-- Sets up mines and turn time
    --SetData("Mine.DudProbability", 0.1)
    --SetData("Mine.MinFuse", 1000)
    --SetData("Mine.MaxFuse", 5000)
    SetData("Land.Indestructable", 1)
    SetupWormsAndTeams()
    SetupInventories()
 
    --SpawnCrate( "kWeaponBazooka" )
    SendMessage("GameLogic.PlaceObjects")
 
    SetData("Camera.StartOfTurnCamera", "Default")
    StartFirstTurn()
    
    nTurnCount = 0
    DoOncePerTurnFunctions()
end


function SetupWormsAndTeams()
-- Activate Team 0
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = "Team 1"
    team.TeamColour = 0
    team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Activate Team 1
    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "Team 2"
    team.TeamColour = 1
    team.IsAIControlled = true
    CloseContainer(lock)

-- Worm 0, Team 0
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = "T1,W1"
    worm.Energy = 100
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    worm.Spawn = "Worm1"
    CloseContainer(lock) 
--~ -- Worm 1, Team 0
--~     CopyContainer("Worm.Data00", "Worm.Data01")
--~     lock, worm = EditContainer("Worm.Data01") 
--~     worm.Name = "T1,W2"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)
--~ -- Worm 2, Team 0
--~     CopyContainer("Worm.Data00", "Worm.Data02")
--~     lock, worm = EditContainer("Worm.Data02") 
--~     worm.Name = "T1,W3"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)
--~ -- Worm 3, Team 0
--~     CopyContainer("Worm.Data00", "Worm.Data03")
--~     lock, worm = EditContainer("Worm.Data03") 
--~     worm.Name = "T1,W4"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)

-- Worm 4, Team 1
    CopyContainer("Worm.Data00", "Worm.Data04")
    lock, worm = EditContainer("Worm.Data04")
    worm.Energy = 100
    worm.Name = "T2,W1"
    worm.TeamIndex = 1
    worm.Spawn = "Worm2"
    CloseContainer(lock)
--~ -- Worm 5, Team 1
--~     CopyContainer("Worm.Data04", "Worm.Data05")
--~     lock, worm = EditContainer("Worm.Data05") 
--~     worm.Name = "T2,W2"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)
--~ -- Worm 6, Team 1
--~     CopyContainer("Worm.Data04", "Worm.Data06")
--~     lock, worm = EditContainer("Worm.Data06") 
--~     worm.Name = "T2,W3"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)
--~ -- Worm 7, Team 1
--~     CopyContainer("Worm.Data04", "Worm.Data07")
--~     lock, worm = EditContainer("Worm.Data07") 
--~     worm.Name = "T2,W4"
--~     worm.Spawn = "spawn"
--~     CloseContainer(lock)

   SendMessage("WormManager.Reinitialise")
end

function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
--~     inventory.Shotgun = -1
--~     inventory.Bazooka = -1
--~     inventory.Grenade = -1
--~     inventory.ClusterGrenade =-1
--~     inventory.Airstrike = -1
--~     inventory.Dynamite = -1
--~     inventory.HolyHandGrenade = -1
--~     inventory.BananaBomb = -1
--~     inventory.Landmine = -1
--~     inventory.HomingMissile = -1
--~     inventory.Sheep = -1
--~     inventory.SuperSheep = -1
--~     inventory.Parachute = -1
--~     inventory.Jetpack = -1
--~     inventory.OldWoman = -1
--~     inventory.Girder = -1
--~     inventory.BridgeKit = -1
--~     inventory.GasCanister = -1
--~     inventory.NinjaRope = -1
--~     inventory.FirePunch = -1
--~     inventory.Prod = -1
--~     inventory.BaseballBat = -1
--~     inventory.Flood = -1
--~     inventory.Redbull = -1
--~     inventory.WeaponFactoryWeapon = -1
--~     inventory.Starburst = -1
--~     inventory.ChangeWorm = -1
--~     inventory.Surrender = -1
--~     inventory.ConcreteDonkey = -1
--~     inventory.ConcreteDonkey = -1
     inventory.SkipGo = -1
     inventory.HomingMissile = -1
     
    CloseContainer(lock) -- must close the container ASAP

    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)

-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Team00")
   CopyContainer("Inventory.Team.Default", "Inventory.Team01")

    CopyContainer("AIParams.CPU5", "AIParams.Worm00")
    CopyContainer("AIParams.CPU5", "AIParams.Worm04")
end

function Crate_Collected()
end

function DoWormpotOncePerTurnFunctions()
end

function SpawnCrate( szContents )
     SetData("Crate.Spawn", "Crate1")
     SetData("Crate.Type", "Weapon")
     SetData("Crate.Contents", szContents ) 
     SetData("Crate.NumContents", 1)
     SetData("Crate.Index", 0)
     SetData("Crate.GroundSnap", 1)
     SendMessage("GameLogic.CreateCrate")
end

function DoOncePerTurnFunctions()
end

function CheckWormHealth( szWormContainerName, nHealthLowerLimit, nHealthUpperLimit )
end











